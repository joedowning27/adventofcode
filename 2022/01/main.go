package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"sort"
	"strconv"
)

type Elf struct {
	index int
	cals  int
}

func main() {
	if len(os.Args) < 2 {
		log.Fatal("Need an input file")
	}
	var input []string
	var elves []Elf
	file, err := os.Open(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	s := bufio.NewScanner(file)
	for s.Scan() {
		input = append(input, s.Text())
	}

	elfNum := 1
	elfCals := 0
	for i := 0; i < len(input); i++ {
		if input[i] == "" {
			elves = append(elves, Elf{elfNum, elfCals})
			elfCals = 0
			elfNum++
			continue
		}
		cals, _ := strconv.ParseInt(input[i], 10, 64)
		elfCals += int(cals)
	}
	sort.Slice(elves, func(i, j int) bool { return elves[i].cals > elves[j].cals })
	fmt.Printf("%v\n", elves[:3])

	totalCals := elves[0].cals + elves[1].cals + elves[2].cals
	fmt.Printf("totalCals: %d\n", totalCals)
}
