package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"
)

var Scorebook1 = map[string]map[string]int{
	"A": {
		"X": 3 + 1,
		"Y": 6 + 2,
		"Z": 0 + 3,
	},
	"B": {
		"X": 0 + 1,
		"Y": 3 + 2,
		"Z": 6 + 3,
	},
	"C": {
		"X": 6 + 1,
		"Y": 0 + 2,
		"Z": 3 + 3,
	},
}
var Scorebook2 = map[string]map[string]int{
	"A": { // r
		"X": 0 + 3, // Lose s
		"Y": 3 + 1, // Draw r
		"Z": 6 + 2, // Win  p
	},
	"B": { // p
		"X": 0 + 1, // Lose r
		"Y": 3 + 2, // Draw p
		"Z": 6 + 3, // Win  s
	},
	"C": { // s
		"X": 0 + 2, // Lose p
		"Y": 3 + 3, // Draw s
		"Z": 6 + 1, // Win  r
	},
}

func score(j string, k string) int {
	return Scorebook2[j][k]
}

func main() {
	if len(os.Args) < 2 {
		log.Fatal("Need an input file")
	}
	var input []string
	file, err := os.Open(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	s := bufio.NewScanner(file)
	for s.Scan() {
		input = append(input, s.Text())
	}

	acc := 0
	for i := 0; i < len(input); i++ {
		strategy := strings.Split(input[i], " ")
		result := score(strategy[0], strategy[1])
		fmt.Printf("Op: %s, Me: %s, Res: %d\n", strategy[0], strategy[1], result)
		acc += result
	}
	fmt.Printf("Score: %d\n", acc)
}
