package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"
)

func ProcessLines(line string) int {
	// split line into two compartments
	comp1 := line[:len(line)/2]
	comp2 := line[len(line)/2:]

	// find duplicate item
	var dup byte
	for i := 0; i < len(comp1); i++ {
		for j := 0; j < len(comp2); j++ {
			if comp1[i] == comp2[j] {
				dup = comp1[i]
			}
		}
	}

	//convert to value
	if dup >= 'a' && dup <= 'z' {
		// ascii a is 97
		return int(dup - 96)

	} else if dup >= 'A' && dup <= 'Z' {
		// ascii A is
		return int(dup - 38)
	}
	return 0
}
func ProcessGroups(packs []string) int {
	fmt.Println(packs)
	if len(packs) != 3 {
		return -1
	}

	// find duplicate item
	var dup byte
	for i := 0; i < len(packs[0]); i++ {
		sub := string(packs[0][i])
		if strings.Contains(packs[1], sub) && strings.Contains(packs[2], sub) {
			dup = byte(sub[0])
		}
	}

	//convert to value
	if dup >= 'a' && dup <= 'z' {
		// ascii a is 97
		return int(dup - 96)

	} else if dup >= 'A' && dup <= 'Z' {
		// ascii A is
		return int(dup - 38)
	}
	return 0
}

func main() {
	if len(os.Args) < 2 {
		log.Fatal("Need an input file")
	}
	var input []string
	file, err := os.Open(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	s := bufio.NewScanner(file)
	for s.Scan() {
		input = append(input, s.Text())
	}

	acc := 0
	for i := 0; i <= len(input)-3; i += 3 {
		acc += ProcessGroups(input[i : i+3])
	}

	fmt.Printf("Score: %d\n", acc)
}
