package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

type Range struct {
	start int
	end   int
}

func IsSubset(a Range, b Range) int {
	// a is a subset of b if a.start >= b.start && a.end <= b.end
	if a.start >= b.start && a.end <= b.end {
		return 1
	}
	// b is a subset if a if a.start <= b.start && a.end >=b.end
	if a.start <= b.start && a.end >= b.end {
		return 1
	}
	return 0
}
func IsOverlap(a Range, b Range) int {
	// a does NOT overlap b if
	if a.start < b.start && a.end < b.start || a.start > b.end && a.end > b.end {
		return 0
	}
	return 1
}

// return 1 if one group is a subset of another
func ProcessLines(line string) int {
	// split into two assignments based on commas
	assignments := strings.Split(line, ",")
	if len(assignments) != 2 {
		log.Fatal("there need to be two assignments")
	}

	// split each assignment into a start and an end
	ranges := []Range{}
	for i := 0; i < len(assignments); i++ {
		points := strings.Split(assignments[i], "-")
		if len(points) != 2 {
			log.Fatal("assignment should be of the form N-N")
		}
		start, _ := strconv.Atoi(points[0])
		end, _ := strconv.Atoi(points[1])
		ranges = append(ranges, Range{start, end})
	}
	// check
	if len(ranges) != 2 {
		log.Fatal("expected to check two ranges")
	}
	a := ranges[0]
	b := ranges[1]
	return IsOverlap(a, b)
}

func main() {
	if len(os.Args) < 2 {
		log.Fatal("Need an input file")
	}
	var input []string
	file, err := os.Open(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	s := bufio.NewScanner(file)
	for s.Scan() {
		input = append(input, s.Text())
	}

	acc := 0
	for i := 0; i < len(input); i++ {
		acc += ProcessLines(input[i])
	}

	fmt.Printf("Score: %d\n", acc)
}
