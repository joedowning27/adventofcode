package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

type Move struct {
	num  int
	from int
	to   int
}

func MoveCrates(s []string, f int, t int, n int) []string {
	item := s[f][:n]
	s[f] = s[f][n:]
	s[t] = item + s[t]
	return s
}

// PART1 crates are moved one at a time
func ProcessMove1(stacks []string, m Move) []string {
	for i := 0; i < m.num; i++ {
		stacks = MoveCrates(stacks, m.from, m.to, 1)
	}
	return stacks
}

// PART2 crates are moved all at once
func ProcessMove2(stacks []string, m Move) []string {
	return MoveCrates(stacks, m.from, m.to, m.num)
}

func main() {
	if len(os.Args) < 2 {
		log.Fatal("Need an input file")
	}
	var input []string
	file, err := os.Open(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	s := bufio.NewScanner(file)
	for s.Scan() {
		input = append(input, s.Text())
	}

	numStacks, _ := strconv.Atoi(input[0])
	stacks := input[1 : numStacks+1]
	moves := input[numStacks+1:]

	for i := 0; i < len(moves); i++ {
		words := strings.Split(moves[i], " ")
		num, _ := strconv.Atoi(words[1])
		from, _ := strconv.Atoi(words[3])
		to, _ := strconv.Atoi(words[5])
		m := Move{num, from - 1, to - 1}
		stacks = ProcessMove2(stacks, m)
	}

	tops := ""
	for i := 0; i < numStacks; i++ {
		tops += stacks[i][:1]
	}
	fmt.Printf("Score: %v\n", tops)
}
