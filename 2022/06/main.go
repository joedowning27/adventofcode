package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"
)

func main() {
	if len(os.Args) < 2 {
		log.Fatal("Need an input file")
	}
	var input string
	file, err := os.Open(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	s := bufio.NewScanner(file)
	for s.Scan() {
		input = input + s.Text()
	}

	fmt.Printf("Input: %s\n", input)
	window := 14
	for i := window; i < len(input); i++ {
		test := input[i-window : i]
		unique := 1
		for j := 0; j < window; j++ {
			if strings.Count(test, string(test[j])) != 1 {
				unique = 0
			}
		}
		if unique == 1 {
			fmt.Printf("Found: %s\n", test)
			fmt.Printf("Score: %d\n", i)
			return
		}
	}
}
