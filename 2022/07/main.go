package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

const largeFile int = 100000
const diskTotal int = 70000000
const updateSize int = 30000000

type Node struct {
	name     string
	isDir    bool
	size     int
	parent   *Node
	children []*Node
}

func (n Node) CalculateSize() int {
	total := 0
	for _, v := range n.children {
		if v.isDir {
			v.size = v.CalculateSize()
		}
		total += v.size
	}
	return total
}
func (n Node) FindSmallestDir(limit int) int {
	min := n.size
	for _, v := range n.children {
		if v.isDir {
			vmin := v.FindSmallestDir(limit)
			if vmin < min && vmin > limit {
				min = vmin
			}
		}
	}
	return min
}

func CalcSmallTotal(n *Node, limit int) int {
	total := 0
	if n.size < limit {
		total += n.size
	}
	for _, v := range n.children {
		if v.isDir {
			total += CalcSmallTotal(v, limit)
		}
	}
	return total
}
func PrintDeleteCandidates(n Node, depth int, limit int) {
	fmt.Printf("-%v\n", n)
	for _, v := range n.children {
		if v.isDir && v.size > limit {
			fmt.Printf("%s", strings.Repeat("  ", depth))
			PrintDeleteCandidates(*v, depth+1, limit)
		}
	}
}
func PrintTree(n Node, depth int) {
	fmt.Printf("-%v\n", n)
	for _, v := range n.children {
		fmt.Printf("%s", strings.Repeat("  ", depth))
		PrintTree(*v, depth+1)
	}
}

func NextCmd(lines []string) (int, []string) {
	for i := 0; i < len(lines); i++ {
		toks := strings.Split(lines[i], " ")
		if toks[0] == "$" {
			return i, lines[i:]
		}
	}
	return -1, []string{}
}

func ProcessLines(lines []string) Node {
	// if the thing begins with a command then we
	root := &Node{
		name: "/",
	}
	curNode := root

	for len(lines) > 0 {
		cur, _ := NextCmd(lines)
		next, rem := NextCmd(lines[1:])
		cmd := lines[cur]
		out := []string{}
		if next != -1 {
			out = lines[1 : next+1]
		} else {
			out = lines[1:]
		}

		toks := strings.Split(cmd, " ")
		switch toks[1] {
		case "cd":
			switch toks[2] {
			case "/":
				curNode = root
			case "..":
				curNode = curNode.parent
			default:
				// get the reference to the child node
				for i := 0; i < len(curNode.children); i++ {
					if curNode.children[i].name == toks[2] {
						curNode = curNode.children[i]
					}
				}
			}
		case "ls":
			//create and add children to the current node
			for i := 0; i < len(out); i++ {
				ts := strings.Split(out[i], " ")
				n := Node{name: ts[1], parent: curNode}
				if ts[0] == "dir" {
					n.isDir = true
				} else {
					n.size, _ = strconv.Atoi(ts[0])
				}
				curNode.children = append(curNode.children, &n)
			}
		}

		lines = rem
	}
	return *root
}

func main() {
	if len(os.Args) < 2 {
		log.Fatal("Need an input file")
	}
	var input []string
	file, err := os.Open(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	s := bufio.NewScanner(file)
	for s.Scan() {
		input = append(input, s.Text())
	}

	rootDir := ProcessLines(input)
	rootDir.size = rootDir.CalculateSize()
	// PrintTree(rootDir, 1)

	diskRemaining := diskTotal - rootDir.size
	diskNeeded := updateSize - diskRemaining
	PrintDeleteCandidates(rootDir, 1, diskNeeded)

	fmt.Printf("Score: \t%v\n", CalcSmallTotal(&rootDir, largeFile))
	fmt.Printf("Need: \t%v\n", diskNeeded)
	fmt.Printf("Found: \t%v\n", rootDir.FindSmallestDir(diskNeeded))

}
