package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"

	"github.com/fatih/color"
)

type Tree struct {
	height  int
	visible bool
}
type Point struct {
	x int
	y int
}

func ParseForest(lines []string) [][]Tree {
	forest := [][]Tree{}
	for i, line := range lines {
		tallest := -1
		forest = append(forest, []Tree{})
		for _, tree := range line {
			height, _ := strconv.Atoi(string(tree))
			visible := false
			if height > tallest {
				visible = true
				tallest = height
			}
			forest[i] = append(forest[i], Tree{height, visible})
		}
	}
	// look left to right
	for i := 0; i < len(forest); i++ {
		tallest := -1
		for j := len(forest[i]) - 1; j >= 0; j-- {
			if forest[i][j].height > tallest {
				forest[i][j].visible = true
				tallest = forest[i][j].height
			}
		}
	}
	// look top to bottom
	for j := 0; j < len(forest); j++ {
		tallest := -1
		for i := 0; i < len(forest[j]); i++ {
			if forest[i][j].height > tallest {
				forest[i][j].visible = true
				tallest = forest[i][j].height
			}
		}
	}
	// look bottom to top
	for j := 0; j < len(forest); j++ {
		tallest := -1
		for i := len(forest[j]) - 1; i >= 0; i-- {
			if forest[i][j].height > tallest {
				forest[i][j].visible = true
				tallest = forest[i][j].height
			}
		}
	}
	return forest
}

func DebugForest(f [][]Tree) {
	for _, row := range f {
		for _, tree := range row {
			if tree.visible {
				color.Set(color.BgGreen, color.FgBlack)
			}
			fmt.Printf("%d", tree.height)
			color.Unset()
		}
		fmt.Print("\n")
	}
}

func main() {
	if len(os.Args) < 2 {
		log.Fatal("Need an input file")
	}
	var input []string
	file, err := os.Open(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	s := bufio.NewScanner(file)
	for s.Scan() {
		input = append(input, s.Text())
	}

	// contains build in left to right comparisons
	forest := ParseForest(input)

	// find how many trees are visible
	visibleCount := 0
	candidates := []Point{}
	for i, row := range forest {
		for j, tree := range row {
			if tree.visible {
				visibleCount++
				// do not consider edges
				if i != 0 && i != len(forest)-1 && j != 0 && j != len(row)-1 {
					candidates = append(candidates, Point{i, j})
				}
			}
		}
	}
	DebugForest(forest)
	fmt.Printf("Score: %v\n", visibleCount)

	//find the most scenic tree
	fmt.Printf("checking %d candidates\n", len(candidates))
	maxSeen := 0
	for _, c := range candidates {
		// look north x -> 0
		ctree := forest[c.x][c.y]
		seen, seenAcc := 0, 1
		for i := c.x - 1; i >= 0; i-- {
			seen++
			ntree := forest[i][c.y]
			if ntree.height >= ctree.height {
				break
			}
		}
		seenAcc = seenAcc * seen

		// look south x -> len(forest)
		seen = 0
		for i := c.x + 1; i < len(forest); i++ {
			seen++
			ntree := forest[i][c.y]
			if ntree.height >= ctree.height {
				break
			}
		}
		seenAcc = seenAcc * seen

		// look west  y -> 0
		seen = 0
		for j := c.y - 1; j >= 0; j-- {
			seen++
			ntree := forest[c.x][j]
			if ntree.height >= ctree.height {
				break
			}
		}
		seenAcc = seenAcc * seen

		// look east  y -> len(forest[i])
		seen = 0
		for j := c.y + 1; j < len(forest[c.y]); j++ {
			seen++
			ntree := forest[c.x][j]
			if ntree.height >= ctree.height {
				break
			}
		}
		seenAcc = seenAcc * seen

		if seenAcc > maxSeen {
			fmt.Printf("c: %v, seenAcc: %v\n", c, seenAcc)
			maxSeen = seenAcc
		}
	}
	fmt.Printf("MaxSceen: %v\n", maxSeen)
}
