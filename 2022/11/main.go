package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"sort"
	"strconv"
)

const MonkeyLines = 7

type Monkey struct {
	items       []int
	inspect     func(int) int
	testLevel   int
	targetTrue  int
	targetFalse int
	tossCount   uint64
}

func TossItems(ms []Monkey, idx int, div int) []Monkey {
	m := &ms[idx]
	for len(m.items) > 0 {
		item := m.items[0]
		worry := m.inspect(item) % div
		target := 0
		if worry%m.testLevel == 0 {
			target = m.targetTrue
		} else {
			target = m.targetFalse
		}
		ms[target].items = append(ms[target].items, worry)
		m.items = m.items[1:]
		m.tossCount++
	}

	return ms
}

func MakeMonkey(lines []string) Monkey {
	var m Monkey
	if len(lines) != MonkeyLines {
		log.Fatalf("Expected %d lines got %d", MonkeyLines, len(lines))
	}

	// lines[1]:   Starting items: ...
	itemsRe := regexp.MustCompile(`(?:Starting items: |, )(\d+)`)
	matches := itemsRe.FindAllStringSubmatch(lines[1], -1)
	for _, match := range matches {
		item, err := strconv.Atoi(match[1])
		if err != nil {
			log.Fatalf("Falied to parse item: %#v", match)
		}
		m.items = append(m.items, item)
	}

	// lines[2]:   Operation: new = old [+*] N
	opRe := regexp.MustCompile(`Operation: new = old ([+*]) (\d+|old)`)
	match := opRe.FindStringSubmatch(lines[2])
	op := match[1]
	val := match[2]
	if val == "old" {
		switch op {
		case "*":
			m.inspect = func(old int) int { return old * old }
		case "+":
			m.inspect = func(old int) int { return old + old }
		default:
			log.Fatalf("Operator '%v' not supported", op)
		}
	} else {
		v, _ := strconv.Atoi(val)
		switch op {
		case "*":
			m.inspect = func(old int) int { return old * v }
		case "+":
			m.inspect = func(old int) int { return old + v }
		default:
			log.Fatalf("Operator '%v' not supported", op)
		}
	}

	// lines[3]:   Test: divisible by N
	testLevelRe := regexp.MustCompile(`Test: divisible by (\d+)`)
	match = testLevelRe.FindStringSubmatch(lines[3])
	level, err := strconv.Atoi(match[1])
	if err != nil {
		log.Fatalf("Falied to parse level: %#v", match)
	}
	m.testLevel = level

	// lines[4]:     If true: throw to monkey N
	testTrueRe := regexp.MustCompile(`If true: throw to monkey (\d+)`)
	match = testTrueRe.FindStringSubmatch(lines[4])
	targetTrue, err := strconv.Atoi(match[1])
	if err != nil {
		log.Fatalf("Falied to parse targetTrue: %#v", match)
	}
	m.targetTrue = targetTrue

	// lines[5]:     If false: throw to monkey N
	testFalseRe := regexp.MustCompile(`If false: throw to monkey (\d+)`)
	match = testFalseRe.FindStringSubmatch(lines[5])
	targetFalse, err := strconv.Atoi(match[1])
	if err != nil {
		log.Fatalf("Falied to parse targetTrue: %#v", match)
	}
	m.targetFalse = targetFalse

	return m
}
func ParseMonkeys(lines []string) []Monkey {
	monkeys := []Monkey{}
	for i := 0; i < len(lines); i += MonkeyLines {
		monkey := MakeMonkey(lines[i : i+MonkeyLines])
		monkeys = append(monkeys, monkey)
	}
	return monkeys
}
func DebugMonkeys(monkeys []Monkey) {
	for i, m := range monkeys {
		fmt.Printf("monkey[%d]: %v\n", i, m)
	}
}

func FindGCD(a, b int) int {
	for b != 0 {
		r := a % b
		a = b
		b = r
	}
	return a
}

func FindLCM(a int, b int, rest ...int) int {
	res := a * b / FindGCD(a, b)
	for _, v := range rest {
		res = FindLCM(res, v)
	}
	return res
}

func main() {
	if len(os.Args) < 2 {
		log.Fatal("Need an input file")
	}
	NumRounds := 20
	if len(os.Args) == 3 {
		NumRounds, _ = strconv.Atoi(os.Args[2])
	}
	var input []string
	file, err := os.Open(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	s := bufio.NewScanner(file)
	for s.Scan() {
		input = append(input, s.Text())
	}

	monkeys := ParseMonkeys(input)

	levels := []int{}
	for _, m := range monkeys {
		levels = append(levels, m.testLevel)
	}

	lcm := FindLCM(levels[0], levels[1], levels[2:]...)

	for r := 0; r < NumRounds; r++ {
		for i := range monkeys {
			monkeys = TossItems(monkeys, i, lcm)
		}
	}

	sort.Slice(monkeys, func(i, j int) bool { return monkeys[i].tossCount > monkeys[j].tossCount })
	DebugMonkeys(monkeys)
	fmt.Printf("FunnyBuisness: %v\n", monkeys[0].tossCount*monkeys[1].tossCount)
}
