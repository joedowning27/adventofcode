package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"time"
)

var Arg2 = 30
var LargeNumber = 1000

type Point struct {
	x, y int
}

func (p Point) String() string {
	return fmt.Sprintf("(%2d,%2d)", p.x, p.y)
}

type Node struct {
	coord  Point
	height int
	edges  []Point
	dist   [][]int
}

func (n Node) String() string {
	var str string
	str += fmt.Sprintf("%s h: %2d e: ", n.coord, n.height)
	for _, e := range n.edges {
		str += fmt.Sprintf("%s, ", e)
	}
	return str
}

type Maze struct {
	grid  [][]Node // the maze itself 		Point{x, y} = grid[y][x]
	start Point    // starting location		height: S = a - 1 = 0
	end   Point    // ending location    			E = z + 1 = 27
}

func (m Maze) String() string {
	var str string
	str += fmt.Sprintf("Start: %v, End: %v\n", m.start, m.end)
	for y, row := range m.grid {
		for x := range row {
			str += fmt.Sprintln(m.grid[y][x])
		}
	}
	return str
}

func SliceContains[K comparable](slice []K, element K) bool {
	for _, val := range slice {
		if element == val {
			return true
		}
	}
	return false
}

func ParseMaze(lines []string) Maze {
	var maze Maze
	// generate nodes
	maze.grid = make([][]Node, len(lines))
	for y, row := range lines {
		maze.grid[y] = make([]Node, len(row))
		for x, r := range row {
			switch r {
			case 'S':
				maze.start = Point{x, y}
				maze.grid[y][x] = Node{coord: Point{x, y}, height: 0}
			case 'E':
				maze.end = Point{x, y}
				maze.grid[y][x] = Node{coord: Point{x, y}, height: 27}
			default:
				maze.grid[y][x] = Node{coord: Point{x, y}, height: int(r) - 96}
			}
		}
	}
	// fill in the edges
	for y, row := range maze.grid {
		for x, node := range row {
			// find the edges for this node
			// up:    {x, y - 1}
			if y > 0 && maze.grid[y-1][x].height <= node.height+1 {
				// maze.grid[y][x].edges = append(maze.grid[y][x].edges, Point{x, y - 1})
				maze.grid[y-1][x].edges = append(maze.grid[y-1][x].edges, Point{x, y})
			}
			// down:  {x, y + 1}
			if y < len(lines)-1 && maze.grid[y+1][x].height <= node.height+1 {
				// maze.grid[y][x].edges = append(maze.grid[y][x].edges, Point{x, y + 1})
				maze.grid[y+1][x].edges = append(maze.grid[y+1][x].edges, Point{x, y})
			}

			// left:  {x - 1, y}
			if x > 0 && maze.grid[y][x-1].height <= node.height+1 {
				// maze.grid[y][x].edges = append(maze.grid[y][x].edges, Point{x - 1, y})
				maze.grid[y][x-1].edges = append(maze.grid[y][x-1].edges, Point{x, y})
			}
			// right: {x + 1, y}
			if x < len(row)-1 && maze.grid[y][x+1].height <= node.height+1 {
				// maze.grid[y][x].edges = append(maze.grid[y][x].edges, Point{x + 1, y})
				maze.grid[y][x+1].edges = append(maze.grid[y][x+1].edges, Point{x, y})
			}
		}
	}

	// generate initial distances for Dijkstra from S
	djikstraPrep(&maze, maze.start)
	return maze
}

func djikstraPrep(maze *Maze, start Point) {
	snode := maze.grid[start.y][start.x]
	snode.dist = make([][]int, len(maze.grid))
	for y, row := range maze.grid {
		snode.dist[y] = make([]int, len(row))
		for x := range row {
			switch {
			case Point{x, y} == start:
				snode.dist[y][x] = 0
			case SliceContains(snode.edges, Point{x, y}):
				snode.dist[y][x] = 1
			default:
				snode.dist[y][x] = LargeNumber
			}

		}
	}
	maze.grid[start.y][start.x] = snode
}

// TODO if this is not fast enough we may have to add a weight/bias based on Point position
// this value is added to the distance but should NOT be more than the normal edge weight (1 is used here)
func djikstra(m *Maze, start Point) {
	var toVisit []Point
	for y, row := range m.grid {
		for x := range row {
			toVisit = append(toVisit, Point{x, y})
		}
	}
	for len(toVisit) > 0 {
		// find the next "closest" node
		cIdx := 0
		cPoint := toVisit[cIdx]
		cMin := m.grid[start.y][start.x].dist[cPoint.y][cPoint.x]
		for i, n := range toVisit {
			if m.grid[start.y][start.x].dist[n.y][n.x] < cMin {
				cPoint = n
				cIdx = i
				cMin = m.grid[start.y][start.x].dist[cPoint.y][cPoint.x]
			}
		}
		// for each edge in the closest node update each edges distances
		for _, e := range m.grid[cPoint.y][cPoint.x].edges {
			// do not update the edge if we have already visited it
			visited := true
			for _, v := range toVisit {
				if e == v {
					visited = false
				}
			}
			if !visited {
				m.grid[start.y][start.x].dist[e.y][e.x] = cMin + 1
			}
		}
		// remove the "closest" node from the search list
		toVisit = append(toVisit[:cIdx], toVisit[cIdx+1:]...)
	}
}

func CalcDist(m *Maze, start Point, dest Point) int {
	// if we have found this distance already return it
	if m.grid[start.y][start.x].dist[dest.y][dest.x] == LargeNumber {
		djikstra(m, start)
	}
	// search through the rest of the graph to find distance to dest
	return m.grid[start.y][start.x].dist[dest.y][dest.x]
}

func main() {
	start := time.Now()
	if len(os.Args) < 2 {
		log.Fatal("Need an input file")
	}
	if len(os.Args) == 3 {
		Arg2, _ = strconv.Atoi(os.Args[2])
	}
	var input []string
	file, err := os.Open(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	s := bufio.NewScanner(file)
	for s.Scan() {
		input = append(input, s.Text())
	}

	m := ParseMaze(input)

	djikstraPrep(&m, m.end)
	dist := CalcDist(&m, m.start, m.end)
	fmt.Printf("Distance: %d\n", dist)
	back := CalcDist(&m, m.end, m.start)
	fmt.Printf("Backwards: %d\n", back)

	var minTrail = LargeNumber
	var candidates []Point
	for y, row := range m.grid {
		for x, n := range row {
			if n.height <= 1 {
				candidates = append(candidates, Point{x, y})
			}
		}
	}
	for _, p := range candidates {
		// n := m.grid[p.y][p.x]
		d := CalcDist(&m, m.end, p)
		if d < minTrail {
			minTrail = d
			// fmt.Println(n)
		}
	}
	fmt.Printf("MinTrail: %d\n", minTrail)
	runtime := time.Since(start)
	log.Printf("day 16 took %s\n", runtime)
}
