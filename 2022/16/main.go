package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"
)

var NumRounds = 30

type Node struct {
	name  string
	open  bool
	rate  int
	edges []string
	dist  map[string]int
}

type Cave struct {
	caverns       map[string]Node // the cave itself
	time          int             // time remaining
	valveRate     int             // amount of steam produced per min
	steamProduced int             // total steam produced
	location      string          // starting location
	myTarget      string          // the valve I am going to open
	elTarget      string          // the valve the elephant is going to open
	myTravel      int             // time until I arrive at my target
	elTravel      int             // time until the elephant arrives at it's target
}

type Action func(Cave) Cave
type pair struct {
	first  string
	second string
}

func MakeNode(line string) Node {
	var n Node
	var err error
	// Valve XX has flow rate=DD; tunnels lead to valves XX, XX, ...
	testLevelRe := regexp.MustCompile(`Valve (.+) has flow rate=(\d+); tunnels? leads? to valves? (.+)`)
	matches := testLevelRe.FindStringSubmatch(line)

	n.name = matches[1]
	n.open = false
	n.rate, err = strconv.Atoi(matches[2])
	if err != nil {
		log.Fatalf("Failed to parse id: %#v", matches)
	}
	paths := strings.Split(matches[3], ", ")
	n.edges = []string{}
	n.dist = map[string]int{}
	for _, k := range paths {
		n.dist[k] = 1
		n.edges = append(n.edges, k)
	}
	return n
}
func ParseNodes(lines []string) map[string]Node {
	nodes := map[string]Node{}
	// generate nodes
	for _, v := range lines {
		n := MakeNode(v)
		nodes[n.name] = n
	}
	// generate initial weights
	for _, s := range nodes {
		for _, d := range nodes {
			switch {
			case s.name == d.name:
				s.dist[d.name] = 0
			case s.dist[d.name] != 0:
				continue
			default:
				s.dist[d.name] = 10 * len(nodes)
			}
		}
	}
	return nodes
}
func DebugNodes(nodes map[string]Node) {
	for _, n := range nodes {
		var status string = "closed"
		if n.open {
			status = "  open"
		}
		var edges = []string{}
		for k, v := range n.dist {
			mark := " "
			for _, e := range n.edges {
				if e == k {
					mark = "*"
				}
			}
			if v < 10*len(nodes) {
				edges = append(edges, fmt.Sprintf("%s'%v':%d", mark, k, v))
			}
		}
		edgstr := strings.Join(edges, ", ")
		fmt.Printf("%v: %v, %2d, %v\n", n.name, status, n.rate, edgstr)
	}
}
func DebugCave(c Cave) {
	fmt.Printf("Time: %2d, Valve: %d, Prod: %d\n", c.time, c.valveRate, c.steamProduced)
	fmt.Printf("Me: %s, %d\n", c.myTarget, c.myTravel)
	fmt.Printf("Me: %s, %d\n", c.elTarget, c.elTravel)
	DebugNodes(c.caverns)
}
func djikstra(c *Cave, start string) {
	var toVisit []string
	for k := range c.caverns {
		toVisit = append(toVisit, k)
	}
	for len(toVisit) > 0 {
		// find the next "closest" node
		cIdx := 0
		cName := toVisit[cIdx]
		cMin := c.caverns[start].dist[cName]
		for i, n := range toVisit {
			if c.caverns[start].dist[n] < cMin {
				cName = n
				cIdx = i
				cMin = c.caverns[start].dist[n]
			}
		}
		// for each edge in the closest node update each edges distances
		for _, e := range c.caverns[cName].edges {
			// do not update the edge if we have already visited it
			visited := true
			for _, v := range toVisit {
				if e == v {
					visited = false
				}
			}
			if !visited {
				c.caverns[start].dist[e] = cMin + 1
			}
		}
		// remove the "closest" node from the search list
		next := append(toVisit[:cIdx], toVisit[cIdx+1:]...)
		toVisit = next
	}
}
func CalcTravel(c *Cave, start string, dest string) int {
	// if we have found this distance already return it
	if c.caverns[start].dist[dest] == 10*len(c.caverns) {
		djikstra(c, start)
	}
	// search through the rest of the graph to find distance to dest
	return c.caverns[start].dist[dest]
}
func MakeOpenValveAction(name string) Action {
	return func(c Cave) Cave {
		var new Cave
		// copy the cave map to the new cave
		new.caverns = make(map[string]Node)
		for k, v := range c.caverns {
			new.caverns[k] = v
		}

		// Ttotal = Ttravel + Topen
		t := CalcTravel(&c, c.location, name) + 1

		// travel to the new valve and open it
		new.steamProduced = c.steamProduced + c.valveRate*t
		new.location = name

		// open the valve in the named cavern
		tmpNode := new.caverns[name]
		tmpNode.open = true
		new.caverns[name] = tmpNode
		new.valveRate = c.valveRate + c.caverns[name].rate

		// decrease time based on how much time this action took
		new.time = c.time - t
		return new
	}
}
func WaitAction(c Cave) Cave {
	var new Cave
	// copy the cave map to the new cave
	new.caverns = make(map[string]Node)
	for k, v := range c.caverns {
		new.caverns[k] = v
	}
	// calculate steam generated based on time remaining
	new.location = c.location
	new.valveRate = c.valveRate
	new.steamProduced = c.steamProduced + c.valveRate*c.time
	new.time = 0
	return new
}
func FindActions(c *Cave) []Action {
	var actions []Action
	// we could open any valve in the cave
	for _, n := range c.caverns {
		// only consider valves that are not open and will produce steam
		if !n.open && n.rate > 0 && CalcTravel(c, c.location, n.name)+1 < c.time {
			actions = append(actions, MakeOpenValveAction(n.name))
		}
	}
	if len(actions) == 0 {
		actions = append(actions, WaitAction)
	}
	return actions
}

func OpenElAction(c Cave) Cave {
	var new Cave = c
	// copy the cave map to a new cave state
	new.caverns = make(map[string]Node)
	for k, v := range c.caverns {
		new.caverns[k] = v
	}
	switch {
	case (c.myTravel < c.elTravel || c.elTravel == 0) && c.myTravel < c.time:
		// I arrive first so open the valve that I am going to
		new.myTravel = 0
		new.elTravel -= c.myTravel
		new.time -= c.myTravel

		//produce steam for the open valves
		new.steamProduced = c.steamProduced + c.valveRate*(c.myTravel)
		//open the target valve
		n := new.caverns[c.myTarget]
		n.open = true
		new.caverns[c.myTarget] = n
		new.valveRate += n.rate

	case (c.elTravel < c.myTravel || c.myTravel == 0) && c.elTravel < c.time:
		// the elephant arrived first, open theirs
		new.elTravel = 0
		new.myTravel -= c.elTravel
		new.time -= c.elTravel

		//produce steam for the open valves
		new.steamProduced = c.steamProduced + c.valveRate*(c.elTravel)
		//open the target valve
		n := new.caverns[c.elTarget]
		n.open = true
		new.caverns[c.elTarget] = n
		new.valveRate += n.rate
	case c.elTravel == c.myTravel && c.elTravel < c.time:
		// we arrive at the same time. open both
		// the elephant arrived first, open theirs
		new.elTravel = 0
		new.myTravel = 0
		new.time -= c.elTravel

		//produce steam for the open valves
		new.steamProduced = c.steamProduced + c.valveRate*(c.elTravel)

		//open the target valves
		m := new.caverns[c.myTarget]
		e := new.caverns[c.elTarget]
		m.open = true
		e.open = true
		new.caverns[c.myTarget] = m
		new.caverns[c.elTarget] = e
		new.valveRate += m.rate + e.rate
	case c.elTravel >= c.time && c.myTravel >= c.time:
		// we cannot open a new valve so we need to wait
		new.time = 0
		new.elTravel -= c.time
		new.myTravel -= c.time

		// produce steam
		new.steamProduced = c.steamProduced + c.valveRate*(c.time)
	default:
		log.Fatal("OOPS, guess you missed a case")
	}
	return new
}

func FindElTargets(c Cave) []pair {
	var options []string
	var pairs []pair
	// be sure to check the other target when assigning, we don't want duplicates
	for k, v := range c.caverns {
		if !v.open && v.rate > 0 {
			options = append(options, k)
		}
	}
	if len(options) <= 0 {
		return pairs
	}
	switch {
	case c.myTravel == 0 && c.elTravel == 0:
		if len(options) == 1 {
			pairs = append(pairs, pair{first: options[0], second: ""})
		}
		for i := range options[:len(options)-1] {
			for j := i + 1; j < len(options); j++ {
				pairs = append(pairs, pair{first: options[i], second: options[j]})
			}
		}
	case c.myTravel != 0 && c.elTravel == 0:
		for _, v := range options {
			// you are already on your way to a valve elephant should open a different one
			if v != c.myTarget {
				pairs = append(pairs, pair{first: "", second: v})
			}
		}
	case c.myTravel == 0 && c.elTravel != 0:
		for _, v := range options {
			// elephant is already on their way to a valve do not duplicate their efforts
			if v != c.elTarget {
				pairs = append(pairs, pair{first: v, second: ""})
			}
		}
	case c.myTravel != 0 && c.elTravel != 0:
		fmt.Printf("End?\n")
		return []pair{}
	}
	return pairs
}

func FindMaxEl(c Cave) int {
	if c.time == 0 {
		if c.valveRate >= 81 && c.time >= 14 {
			fmt.Printf("GREATER THAN EXPECTED MAX\n")
		}
		return c.steamProduced
	}
	// for the given cave find new targets
	// for each possible action find its best solution
	maxSteam := 0
	tgts := FindElTargets(c)

	// if there are no targets then we need to wait
	if len(tgts) <= 0 {
		w := WaitAction(c)
		if w.valveRate >= 81 && w.time >= 14 {
			fmt.Printf("GREATER THAN EXPECTED MAX\n")
		}
		return w.steamProduced
	}

	for _, p := range tgts {
		if p.first != "" {
			c.myTravel = CalcTravel(&c, c.myTarget, p.first) + 1
			c.myTarget = p.first
		}
		if p.second != "" {
			c.elTravel = CalcTravel(&c, c.elTarget, p.second) + 1
			c.elTarget = p.second
		}
		n := OpenElAction(c)
		tmp := FindMaxEl(n)
		if n.valveRate >= 81 && n.time >= 14 {
			fmt.Printf("GREATER THAN EXPECTED MAX\n")
		}
		if tmp > maxSteam {
			maxSteam = tmp
		}

	}
	if c.valveRate >= 81 && c.time >= 14 {
		fmt.Printf("GREATER THAN EXPECTED MAX\n")
	}
	return maxSteam
}

func FindMax(c Cave) int {
	if c.time == 0 {
		return c.steamProduced
	}
	// for each possible action find its best solution
	maxSteam := 0
	for _, act := range FindActions(&c) {
		n := act(c)
		tmp := FindMax(n)
		if tmp > maxSteam {
			maxSteam = tmp
		}
	}
	return maxSteam
}

func main() {
	start := time.Now()
	if len(os.Args) < 2 {
		log.Fatal("Need an input file")
	}
	if len(os.Args) == 3 {
		NumRounds, _ = strconv.Atoi(os.Args[2])
	}
	var input []string
	file, err := os.Open(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	s := bufio.NewScanner(file)
	for s.Scan() {
		input = append(input, s.Text())
	}

	nodes := ParseNodes(input)
	c := Cave{
		time:          NumRounds,
		caverns:       nodes,
		valveRate:     0,
		steamProduced: 0,
		location:      "AA",
		myTarget:      "AA",
		elTarget:      "AA",
		myTravel:      0,
		elTravel:      0,
	}

	// max := FindMax(c)
	max := FindMaxEl(c)
	fmt.Printf("SteamProduced: %d\n", max)
	runtime := time.Since(start)
	log.Printf("day 16 took %s\n", runtime)
}
