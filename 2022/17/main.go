package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
	"time"
)

// ####
var widePiece = []string{
	"..@@@@.",
}

// .#.
// ###
// .#.
var plusPiece = []string{
	"...@...",
	"..@@@..",
	"...@...",
}

// ..#
// ..#
// ###
var letterPiece = []string{
	"....@..",
	"....@..",
	"..@@@..",
}

// #
// #
// #
// #
var tallPiece = []string{
	"..@....",
	"..@....",
	"..@....",
	"..@....",
}

// ##
// ##
var sqrPiece = []string{
	"..@@...",
	"..@@...",
}

var emptySpace = "......."
var floorSpace = "-------"
var emptySpaces = []string{emptySpace, emptySpace, emptySpace}
var MaxPieceHeight = 4
var Pieces = [][]string{widePiece, plusPiece, letterPiece, tallPiece, sqrPiece}

const DefRounds = 2022

type point struct {
	x int
	y int
}

func PrintBoard(b *[]string) {
	fmt.Println("Board:")
	for _, row := range *b {
		if row == floorSpace {
			fmt.Printf("+%s+\n", row)
		} else {
			fmt.Printf("|%s|\n", row)
		}
	}
}

// A rock is resting if any @ is directly above '-' or '#'
func FallRock(board *[]string, height int) bool {
	switch {
	case (*board)[height] == floorSpace:
		return false
	case (*board)[height] == emptySpace:
		*board = append((*board)[:height], (*board)[height+1:]...)
		return true
	default:
		// if the falling rock rests on the next rock then it stops falling
		for y := height - 1; y >= 0; y-- {
			for i := 0; i < len((*board)[y]); i++ {
				if (*board)[y][i] == '@' && (*board)[y+1][i] == '#' {
					return false
				}
			}
		}
		// if the falling rock does not overlap then it should fall past other rocks
		//TODO: the bug is here, we should keep track of where the @s are, not try and bring the # "up"
		var idxs []point
		for y, row := range (*board)[:height] {
			for x, c := range row {
				if c == '@' {
					idxs = append(idxs, point{x, y})
					tmp := []byte((*board)[y])
					tmp[x] = '.'
					(*board)[y] = string(tmp)
				}
			}
		}
		for _, p := range idxs {
			tmp := []byte((*board)[p.y+1])
			tmp[p.x] = '@'
			(*board)[p.y+1] = string(tmp)
		}
		if (*board)[0] == emptySpace {
			*board = (*board)[1:]
			return true
		}
		return true
	}
}

func PushRock(board *[]string, wind byte, height int) {

	switch wind {
	case '>':
		for _, row := range (*board)[:height] {
			ridx := strings.LastIndex(row, "@")
			if ridx == -1 {
				continue
			}
			// we are blocked if the rock is against the wall
			// or if it is up against another rock
			if ridx == len(row)-1 || row[ridx+1] == '#' {
				return
			}
		}
		for i, row := range (*board)[:height] {
			idxs := []int{}
			for idx, c := range row {
				if c == '@' {
					idxs = append(idxs, idx)
				}
			}
			tmp := []rune(row)
			for _, idx := range idxs {
				tmp[idx] = '.'
			}
			for _, idx := range idxs {
				tmp[idx+1] = '@'
			}
			// fmt.Printf("%s > %s\n", (*board)[i], string(tmp))
			(*board)[i] = string(tmp)
		}
	case '<':
		for _, row := range (*board)[:height] {
			ridx := strings.Index(row, "@")
			if ridx == -1 {
				continue
			}
			// we are blocked if the rock is against the wall
			// or if it is up against another rock
			if ridx == 0 || row[ridx-1] == '#' {
				return
			}
		}
		for i, row := range (*board)[:height] {
			idxs := []int{}
			for idx, c := range row {
				if c == '@' {
					idxs = append(idxs, idx)
				}
			}
			tmp := []rune(row)
			for _, idx := range idxs {
				tmp[idx] = '.'
			}
			for _, idx := range idxs {
				tmp[idx-1] = '@'
			}
			// fmt.Printf("%s < %s\n", (*board)[i], string(tmp))
			(*board)[i] = string(tmp)
		}
	default:
		log.Fatalf("Invalid Wind Value '%v'\n", wind)
	}
	return
}

func SettleRock(board []string) []string {
	for i, row := range board {
		board[i] = strings.ReplaceAll(row, "@", "#")
	}
	return board
}

func FindHeight(board []string) int {
	height := -1
	for y, row := range board {
		if strings.Contains(row, "@") {
			height = y
		}
		if height >= 0 && !strings.Contains(row, "@") {
			break
		}
	}
	return height + 1
}

func DropRock(board []string, rock []string, wind string) ([]string, string) {
	// 1. rock starts falling
	board = append(emptySpaces, board...)
	board = append(rock, board...)
	falling := true
	for {
		// PrintBoard(board)
		// fmt.Printf("wind: %s\n", wind)
		// find the lowest index of the rock
		height := FindHeight(board)
		// PrintBoard(board[:height])
		// 2. rock gets pushed
		PushRock(&board, wind[0], height)
		wind = string(append([]byte(wind[1:]), wind[0]))
		// 3. rock falls one level
		falling = FallRock(&board, height)
		if !falling {
			break
		}
	}
	// 4. solidify rocks (convert @ to #)
	board = SettleRock(board)
	return board, wind
}

func main() {
	start := time.Now()
	if len(os.Args) < 2 {
		log.Fatal("Need an input file")
	}
	NumRounds := DefRounds
	if len(os.Args) == 3 {
		NumRounds, _ = strconv.Atoi(os.Args[2])
	}

	var input []string
	file, err := os.Open(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	s := bufio.NewScanner(file)
	for s.Scan() {
		input = append(input, s.Text())
	}
	board := []string{floorSpace}
	wind := input[0]
	Prog := NumRounds / 100
	for i := 0; i < NumRounds; i++ {
		if i%Prog == 0 {
			fmt.Print(".")
		}
		bid := i % len(Pieces)
		// fmt.Printf("======= ROUND %d ========= ", i)
		board, wind = DropRock(board, Pieces[bid], wind)
		// PrintBoard(board)
		// fmt.Printf("wind: %s\n", wind)
	}
	fmt.Printf("\nTower Height: %v\n", len(board)-1)
	runtime := time.Since(start)
	log.Printf("day 17 took %s", runtime)
}
