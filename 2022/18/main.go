package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"

	"github.com/fatih/color"
)

type Point struct {
	full  bool
	steam int
	faces int
	x     int
	y     int
	z     int
}
type Scan [][][]Point

var FaceColors []color.Attribute = []color.Attribute{
	color.FgHiWhite,
	color.FgHiCyan,
	color.FgHiMagenta,
	color.FgHiBlue,
	color.FgHiYellow,
	color.FgHiGreen,
	color.FgHiRed,
}
var SteamColors []color.Attribute = []color.Attribute{
	color.BgBlack,
	color.BgCyan,
}

func FillScan(lines []string) Scan {
	var points []Point
	for _, v := range lines {
		coords := strings.Split(v, ",")
		if len(coords) != 3 {
			log.Fatalf("Could not parse point: %v got: %v\n", v, coords)
		}
		p := Point{}
		p.full = true
		p.x, _ = strconv.Atoi(coords[0])
		p.y, _ = strconv.Atoi(coords[1])
		p.z, _ = strconv.Atoi(coords[2])
		points = append(points, p)
	}
	max := Point{false, 0, 0, 0, 0, 0}
	for _, p := range points {
		if p.x > max.x {
			max.x = p.x
		}
		if p.y > max.y {
			max.y = p.y
		}
		if p.z > max.z {
			max.z = p.z
		}
	}
	fmt.Printf("Max: %v\n", max)
	scan := make([][][]Point, max.x+1)
	for x := 0; x <= max.x; x++ {
		scan[x] = make([][]Point, max.y+1)
		for y := 0; y <= max.y; y++ {
			scan[x][y] = make([]Point, max.z+1)
		}
	}
	for _, p := range points {
		scan[p.x][p.y][p.z] = p
	}
	return scan
}

func CountPointFaces(s Scan, x int, y int, z int) int {
	f := 6
	if x > 0 && (s[x-1][y][z].full || s[x-1][y][z].steam == 0) {
		f--
	}
	if x < len(s)-1 && (s[x+1][y][z].full || s[x+1][y][z].steam == 0) {
		f--
	}
	if y > 0 && (s[x][y-1][z].full || s[x][y-1][z].steam == 0) {
		f--
	}
	if y < len(s[x])-1 && (s[x][y+1][z].full || s[x][y+1][z].steam == 0) {
		f--
	}
	if z > 0 && (s[x][y][z-1].full || s[x][y][z-1].steam == 0) {
		f--
	}
	if z < len(s[x][y])-1 && (s[x][y][z+1].full || s[x][y][z+1].steam == 0) {
		f--
	}
	return f
}

func CountOpenFaces(s Scan) int {
	acc := 0
	for x := range s {
		for y := range s[x] {
			for z := range s[x][y] {
				if s[x][y][z].full {
					s[x][y][z].faces = CountPointFaces(s, x, y, z)
					acc += s[x][y][z].faces
				}
			}
		}
	}
	return acc
}

func WillSteam(s Scan, x int, y int, z int) int {
	f := 0
	//steam spaces must be empty
	if !s[x][y][z].full {
		// if a space is on the edge then it is steam
		if x == 0 || x == len(s)-1 {
			f = 1
		}
		if y == 0 || y == len(s[x])-1 {
			f = 1
		}
		if z == 0 || z == len(s[x][y])-1 {
			f = 1
		}

		// if a neighbor is steam then it is steam
		if x > 0 && s[x-1][y][z].steam > 0 {
			f = 1
		}
		if x < len(s)-1 && s[x+1][y][z].steam > 0 {
			f = 1
		}
		if y > 0 && s[x][y-1][z].steam > 0 {
			f = 1
		}
		if y < len(s[x])-1 && s[x][y+1][z].steam > 0 {
			f = 1
		}
		if z > 0 && s[x][y][z-1].steam > 0 {
			f = 1
		}
		if z < len(s[x][y])-1 && s[x][y][z+1].steam > 0 {
			f = 1
		}
	}
	return f
}

func FillSteam(s Scan) Scan {
	// if an empty space is touching the edge or a neighbor is seam then this space becomes steam
	for x := range s {
		for y := range s[x] {
			for z := range s[x][y] {
				if !s[x][y][z].full {
					s[x][y][z].steam = WillSteam(s, x, y, z)
				}
			}
		}
	}
	for x := len(s) - 1; x >= 0; x-- {
		for y := len(s[x]) - 1; y >= 0; y-- {
			for z := len(s[x][y]) - 1; z >= 0; z-- {
				if !s[x][y][z].full {
					s[x][y][z].steam = WillSteam(s, x, y, z)
				}
			}
		}
	}
	return s
}

func PrintScan(s Scan) {
	for x := range s {
		fmt.Printf("%s\n", strings.Repeat("=", len(s[0][0])))
		for y := range s[x] {
			for z := range s[x][y] {
				color.Set(SteamColors[s[x][y][z].steam], FaceColors[s[x][y][z].faces])
				if s[x][y][z].full {
					fmt.Print(s[x][y][z].faces)
				} else {
					fmt.Print(" ")
				}
				color.Unset()
			}
			fmt.Print("\n")
		}
	}
}

func main() {
	if len(os.Args) < 2 {
		log.Fatal("Need an input file")
	}
	var input []string
	file, err := os.Open(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	s := bufio.NewScanner(file)
	for s.Scan() {
		input = append(input, s.Text())
	}
	scan := FillScan(input)
	scan = FillSteam(scan)
	surfArea := CountOpenFaces(scan)
	PrintScan(scan)
	fmt.Printf("Faces: %v\n", surfArea)
}
