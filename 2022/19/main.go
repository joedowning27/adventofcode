package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
	"sync"
	"time"
)

const DefRounds = 24

type Resource int

const (
	Ore       Resource = 0
	Clay      Resource = 1
	Obsidian  Resource = 2
	Geode     Resource = 3
	RESOURCES Resource = 4
)

type Cost struct {
	cost []int
}
type Blueprint struct {
	id   int
	bots []Cost
}

func MakeBlueprint(line string) Blueprint {
	bots := make([]Cost, RESOURCES)
	for i := range bots {
		bots[i].cost = make([]int, RESOURCES)
	}
	b := Blueprint{0, bots}
	testLevelRe := regexp.MustCompile(`Blueprint (\d+): ` +
		`Each ore robot costs (\d+) ore\. ` +
		`Each clay robot costs (\d+) ore\. ` +
		`Each obsidian robot costs (\d+) ore and (\d+) clay\. ` +
		`Each geode robot costs (\d+) ore and (\d+) obsidian\.`)
	matches := testLevelRe.FindStringSubmatch(line)

	b.id, _ = strconv.Atoi(matches[1])
	b.bots[Ore].cost[Ore], _ = strconv.Atoi(matches[2])
	b.bots[Clay].cost[Ore], _ = strconv.Atoi(matches[3])
	b.bots[Obsidian].cost[Ore], _ = strconv.Atoi(matches[4])
	b.bots[Obsidian].cost[Clay], _ = strconv.Atoi(matches[5])
	b.bots[Geode].cost[Ore], _ = strconv.Atoi(matches[6])
	b.bots[Geode].cost[Obsidian], _ = strconv.Atoi(matches[7])
	return b
}
func ParseBlueprints(lines []string) []Blueprint {
	bps := []Blueprint{}
	for _, v := range lines {
		b := MakeBlueprint(v)
		bps = append(bps, b)
	}
	return bps
}
func DebugBlueprints(bps []Blueprint) {
	for i, b := range bps {
		fmt.Printf("monkey[%d]: %#v\n", i, b)
	}
}

type Action func(s State) State

type State struct {
	time   int   // time remaining
	maxGeo int   // the maximum geodes produced that this state has seen
	bots   []int // number of bots of each type that we have
	items  []int // number of each item we possess
}

/*
	This memoization approach is not going to work, There is not nearly enough overlap
	Maybe we could seperate out the resources and only consider the time and bots. Since they
	are more important for geode production. This gets tricky, we may lose memoization since
	resources effect what we can build and would therefore result in different branches being taken
	Therefore not all states would give the same result with a given set of resources

	Maybe we can try a DAG each node would be a point in time.
	The edges of this node represent actions and are determined by the resources we have available at the time.
	Resources at the next node are determined by the edges we have walked to reach a given node.
	We should favor nodes that get us closer to crafting more geodes
	1. create geo bot
	2. create obsidian bot
	3. create ore/clay bot
	4. wait (this may have a variable weight, if we are 1 ore away from crafting a geode bot it would be best to wait)

	Look ahead? that way we can determine if waiting is a better option

	Prune branches if:
		- we have already seen a geo branch but did not take it
		&&
		- time remaining < geo seen on that branch
	Taking geo is always better when it is available, this may end up being built into our decision algo

	More ways to prune branches
	1. If production of a resource exceeds highest resource cost, do not build that robot
		- Ore prod = 4/min && maxOreCost = 4, then we can build 1 bot per turn, anything more does not help
		- Production = mun of bots of that resource
	2. If we will have more resources than we can spend do not craft that robot
		- resources at end of time: R = bots[r] * t + items[r]
		- resources we can spend: S = max(bots[x].cost[r]) * t
		- If R >= S then do not craft more of that robot
	3. If we can build a geode bot we will always build it
		- We are maximizing for geodes so waiting a min or producing something else will always cost us
		- Should this extend to any bot?
	4. If the maximum geodes we can produce with the time remaining is less than a max we have already seen no sense in continuing
		- pretend we can make a geode robot every turn for the rest of the game
		- if we do not have enough time to overcome max stop
			- TGeode = FGeodes + items[r]
			- FGeodes = t/2 * (2 * bots[Geodes] + t)
	5. Revamp choices, pick what to build next, not what action to take
		- once we pick what we want to build you can "fast-forward" to the point where we can build it
		- There is no more do nothing, unless we run out of time
*/

// check if we can craft a resource
func CanCraft(s State, bpt Blueprint, r Resource) bool {
	for t := range bpt.bots[r].cost {
		if s.items[t] < bpt.bots[r].cost[t] {
			return false
		}
	}
	return true
}

// if we have maxed our production for a resource, no need to make more
func ProductionMaxed(s State, bpt Blueprint, r Resource) bool {
	maxCost := 0
	for _, b := range bpt.bots {
		if b.cost[r] > maxCost {
			maxCost = b.cost[r]
		}
	}
	// fmt.Printf("Production Check: res %v, have %v, max %v\n", r, s.bots[r], maxCost)
	return s.bots[r] >= maxCost
}

// don't waste resources if crafting the bot will result in wasted resources don't do it
// resources are wasted if we have leftover at the end of time
func NeedBot(s State, bpt Blueprint, r Resource) bool {
	// our total possible spend for this resource
	maxCost := 0
	for _, b := range bpt.bots {
		if b.cost[r] > maxCost {
			maxCost = b.cost[r]
		}
	}
	maxSpend := maxCost * s.time
	// get total possible production
	maxProduced := s.bots[r]*s.time + s.items[r]
	return maxSpend > maxProduced
}

func CanBeat(s State, bpt Blueprint) bool {
	// - if we do not have enough time to overcome max stop
	// FGeodes = t/2 * (2 * bots[Geode] + t) + items[Geode]
	theoGeo := ((s.time / 2) * (2*s.bots[Geode] + s.time)) + s.items[Geode]
	return theoGeo > s.maxGeo
}

func makeCraftAction(bpt Blueprint, r Resource) Action {
	return func(s State) State {
		i := make([]int, len(s.items))
		b := make([]int, len(s.bots))
		// calculate resources for next step N' = N - Spent + Mined
		for idx := range s.items {
			i[idx] = s.items[idx] - bpt.bots[r].cost[idx] + s.bots[idx]
		}
		// increment bot count based on resource type
		copy(b, s.bots)
		b[r] = s.bots[r] + 1
		// return the new state with decremented time
		return State{time: s.time - 1, bots: b, items: i}
	}
}

func WaitAction(s State) State {
	i := make([]int, len(s.items))
	b := make([]int, len(s.bots))
	// calculate resources for next step N' = N + Mined
	for idx := range s.items {
		i[idx] = s.items[idx] + s.bots[idx]
	}
	// increment bot count based on resource type
	copy(b, s.bots)
	// return the new state with decremented time
	return State{time: s.time - 1, bots: b, items: i}
}

func FindActions(s State, bpt Blueprint) []Action {
	// if we can craft a geode bot we will only ever do that
	if CanCraft(s, bpt, Geode) {
		return []Action{makeCraftAction(bpt, Geode)}
	}
	// if we cannot beat the current max then give up
	if !CanBeat(s, bpt) {
		return []Action{}
	}
	// if we can afford a bot then add it as an option
	actions := []Action{}
	for r := Ore; r < Geode; r++ {
		if CanCraft(s, bpt, r) && NeedBot(s, bpt, r) && !ProductionMaxed(s, bpt, r) {
			actions = append(actions, makeCraftAction(bpt, r))
		}
	}
	// we can always do nothing
	actions = append(actions, WaitAction)
	return actions
}

func FindMax(bpt Blueprint, s State) int {
	if s.time == 0 {
		return s.items[Geode]
	}
	actions := FindActions(s, bpt)
	for _, a := range actions {
		n := a(s)
		tmp := FindMax(bpt, n)
		if tmp > s.maxGeo {
			s.maxGeo = tmp
		}
	}
	return s.maxGeo
}

func main() {
	start := time.Now()
	if len(os.Args) < 2 {
		log.Fatal("Need an input file")
	}
	NumRounds := DefRounds
	if len(os.Args) == 3 {
		NumRounds, _ = strconv.Atoi(os.Args[2])
	}
	var input []string
	file, err := os.Open(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	s := bufio.NewScanner(file)
	for s.Scan() {
		input = append(input, s.Text())
	}

	bps := ParseBlueprints(input)

	type Result struct {
		id  int
		geo int
	}
	res := make(chan Result, len(bps))
	wg := sync.WaitGroup{}
	for _, bpt := range bps {
		wg.Add(1)
		goFindMax := func(bpt Blueprint, res chan Result) {
			defer wg.Done()
			max := FindMax(bpt, State{time: NumRounds, bots: []int{1, 0, 0, 0}, items: []int{0, 0, 0, 0}})
			res <- Result{id: bpt.id, geo: max}
		}
		go goFindMax(bpt, res)
	}

	wg.Wait()

	acc := 0
	acc2 := 1
	for i := 0; i < len(bps); i++ {
		r := <-res
		fmt.Printf("id: %v, max: %v\n", r.id, r.geo)
		acc += r.id * r.geo
		acc2 *= r.geo
	}
	fmt.Printf("Overall Quality: %v\n", acc)
	fmt.Printf("Geode Mult: %v\n", acc2)
	// DebugBlueprints(bps)
	runtime := time.Since(start)
	log.Printf("day 19 took %s", runtime)
}
