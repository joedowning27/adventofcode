package main

import (
	"fmt"
	"os"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/joedowning27/adventofcode/day01"
)

var (
	checkWordsFlag bool
	runCmd         = &cobra.Command{
		Use:   "day01 [input_file]",
		Args:  cobra.ExactArgs(1),
		Short: "Run the solution for day 1",
		RunE:  solveDay01,
	}
)

func init() {
	runCmd.Flags().BoolVarP(&checkWordsFlag, "words", "w", false, "parse words as numbers")
	solveCmd.AddCommand(runCmd)
}

func solveDay01(cmd *cobra.Command, args []string) error {
	machine_config, err := os.ReadFile(args[0])
	if err != nil {
		return err
	}

	// split the config by line
	lines := strings.Split(string(machine_config), "\n")
	calcTotal := day01.GetCalibrationTotal(lines, checkWordsFlag)

	fmt.Printf("calcTotal: %v\n", calcTotal)
	return nil
}
