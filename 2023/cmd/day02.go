package main

import (
	"fmt"
	"os"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/joedowning27/adventofcode/day02"
)

var (
	redCubes   int64
	greenCubes int64
	blueCubes  int64
	day02Cmd   = &cobra.Command{
		Use:   "day02 [input_file]",
		Args:  cobra.ExactArgs(1),
		Short: "Run the solution for day 2",
		RunE:  solveDay02,
	}
)

func init() {
	// XXX make run part two a global solve flag
	day02Cmd.Flags().BoolVarP(&runPartTwo, "part2", "p", false, "run part two")
	day02Cmd.Flags().Int64VarP(&redCubes, "red", "r", 12, "number of red cubes")
	day02Cmd.Flags().Int64VarP(&greenCubes, "green", "g", 13, "number of green cubes")
	day02Cmd.Flags().Int64VarP(&blueCubes, "blue", "b", 14, "number of blue cubes")
	solveCmd.AddCommand(day02Cmd)

}

func solveDay02(cmd *cobra.Command, args []string) error {
	machine_config, err := os.ReadFile(args[0])
	if err != nil {
		return err
	}

	bag := map[string]int64{
		"red":   redCubes,
		"green": greenCubes,
		"blue":  blueCubes,
	}

	// split the config by line
	lines := strings.Split(string(machine_config), "\n")

	var calcTotal int64 = 0
	if runPartTwo {
		calcTotal = day02.GetCubePowerTotal(lines)
	} else {
		calcTotal = day02.GetCubeGameTotal(lines, bag)
	}

	fmt.Printf("GameTotal: %v\n", calcTotal)
	return nil
}
