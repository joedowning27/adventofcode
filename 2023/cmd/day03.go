package main

import (
	"fmt"
	"os"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/joedowning27/adventofcode/day03"
)

var (
	day03Cmd = &cobra.Command{
		Use:   "day03 [input_file]",
		Args:  cobra.ExactArgs(1),
		Short: "Run the solution for day 3",
		RunE:  solveDay03,
	}
)

func init() {
	day03Cmd.Flags().BoolVarP(&runPartTwo, "part2", "p", false, "run part two")
	solveCmd.AddCommand(day03Cmd)

}

func solveDay03(cmd *cobra.Command, args []string) error {
	machine_config, err := os.ReadFile(args[0])
	if err != nil {
		return err
	}

	// split the config by line
	lines := strings.Split(string(machine_config), "\n")

	var calcTotal = 0
	if runPartTwo {
		calcTotal = day03.GetGearsTotal(lines)
		fmt.Printf("GearsTotal: %v\n", calcTotal)
	} else {
		calcTotal = day03.GetSerialTotal(lines)
		fmt.Printf("SerialTotal: %v\n", calcTotal)
	}

	return nil
}
