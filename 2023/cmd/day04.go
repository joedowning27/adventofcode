package main

import (
	"fmt"
	"os"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/joedowning27/adventofcode/day04"
)

var (
	day04Cmd = &cobra.Command{
		Use:   "day04 [input_file]",
		Args:  cobra.ExactArgs(1),
		Short: "Run the solution for day 4",
		RunE:  solveDay04,
	}
)

func init() {
	day04Cmd.Flags().BoolVarP(&runPartTwo, "part2", "p", false, "run part two")
	solveCmd.AddCommand(day04Cmd)
}

func solveDay04(cmd *cobra.Command, args []string) error {
	machine_config, err := os.ReadFile(args[0])
	if err != nil {
		return err
	}

	// split the config by line
	lines := strings.Split(string(machine_config), "\n")

	ScratchcardsSolution := 0
	if runPartTwo {
		ScratchcardsSolution = day04.GetScratchcardsPart2(lines)
	} else {
		ScratchcardsSolution = day04.GetScratchcardsPart1(lines)
	}

	fmt.Printf("ScratchcardsSolution: %v\n", ScratchcardsSolution)
	return nil
}
