package main

import (
	"fmt"
	"os"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/joedowning27/adventofcode/day05"
)

var (
	day05Cmd = &cobra.Command{
		Use:   "day05 [input_file]",
		Args:  cobra.ExactArgs(1),
		Short: "Run the solution for day 5",
		RunE:  solveDay05,
	}
)

func init() {
	day05Cmd.Flags().BoolVarP(&runPartTwo, "part2", "p", false, "run part two")
	solveCmd.AddCommand(day05Cmd)
}

func solveDay05(cmd *cobra.Command, args []string) error {
	machine_config, err := os.ReadFile(args[0])
	if err != nil {
		return err
	}

	// split the config by line
	lines := strings.Split(string(machine_config), "\n")

	FarmingSolution := 0
	if runPartTwo {
		FarmingSolution = day05.GetFarmingPart2(lines)
	} else {
		FarmingSolution = day05.GetFarmingPart1(lines)
	}

	fmt.Printf("FarmingSolution: %v\n", FarmingSolution)
	return nil
}
