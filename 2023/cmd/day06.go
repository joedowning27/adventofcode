package main

import (
	"fmt"
	"os"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/joedowning27/adventofcode/day06"
)

var (
	day06Cmd = &cobra.Command{
		Use:   "day06 [input_file]",
		Args:  cobra.ExactArgs(1),
		Short: "Run the solution for day 6",
		RunE:  solveDay06,
	}
)

func init() {
	day06Cmd.Flags().BoolVarP(&runPartTwo, "part2", "p", false, "run part two")
	solveCmd.AddCommand(day06Cmd)
}

func solveDay06(cmd *cobra.Command, args []string) error {
	machine_config, err := os.ReadFile(args[0])
	if err != nil {
		return err
	}

	// split the config by line
	lines := strings.Split(string(machine_config), "\n")

	boatRaceSolution := 0
	if runPartTwo {
		boatRaceSolution = day06.GetBoatRacePart2(lines)
	} else {
		boatRaceSolution = day06.GetBoatRacePart1(lines)
	}

	fmt.Printf("boatRaceSolution: %v\n", boatRaceSolution)
	return nil
}
