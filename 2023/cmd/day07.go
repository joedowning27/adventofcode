package main

import (
	"fmt"
	"os"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/joedowning27/adventofcode/day07"
)

var (
	day07Cmd = &cobra.Command{
		Use:   "day07 [input_file]",
		Args:  cobra.ExactArgs(1),
		Short: "Run the solution for day 7",
		RunE:  solveDay07,
	}
)

func init() {
	day07Cmd.Flags().BoolVarP(&runPartTwo, "part2", "p", false, "run part two")
	solveCmd.AddCommand(day07Cmd)
}

func solveDay07(cmd *cobra.Command, args []string) error {
	machine_config, err := os.ReadFile(args[0])
	if err != nil {
		return err
	}

	// split the config by line
	lines := strings.Split(string(machine_config), "\n")

	camelCardsSolution := 0
	if runPartTwo {
		camelCardsSolution = day07.GetCamelcardsPart2(lines)
	} else {
		camelCardsSolution = day07.GetCamelcardsPart1(lines)
	}

	fmt.Printf("camelCardsSolution: %v\n", camelCardsSolution)
	return nil
}