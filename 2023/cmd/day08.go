package main

import (
	"fmt"
	"os"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/joedowning27/adventofcode/day08"
)

var (
	day08Cmd = &cobra.Command{
		Use:   "day08 [input_file]",
		Args:  cobra.ExactArgs(1),
		Short: "Run the solution for day 8",
		RunE:  solveDay08,
	}
)

func init() {
	day08Cmd.Flags().BoolVarP(&runPartTwo, "part2", "p", false, "run part two")
	solveCmd.AddCommand(day08Cmd)
}

func solveDay08(cmd *cobra.Command, args []string) error {
	machine_config, err := os.ReadFile(args[0])
	if err != nil {
		return err
	}

	// split the config by line
	lines := strings.Split(string(machine_config), "\n")

	HauntedWastelandSolution := 0
	if runPartTwo {
		HauntedWastelandSolution = day08.GetHauntedWastelandPart2(lines)
	} else {
		HauntedWastelandSolution = day08.GetHauntedWastelandPart1(lines)
	}

	fmt.Printf("HauntedWastelandSolution: %v\n", HauntedWastelandSolution)
	return nil
}
