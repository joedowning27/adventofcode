package main

import (
	"fmt"
	"os"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/joedowning27/adventofcode/day09"
)

var (
	day09Cmd = &cobra.Command{
		Use:   "day09 [input_file]",
		Args:  cobra.ExactArgs(1),
		Short: "Run the solution for day 9",
		RunE:  solveDay09,
	}
)

func init() {
	day09Cmd.Flags().BoolVarP(&runPartTwo, "part2", "p", false, "run part two")
	solveCmd.AddCommand(day09Cmd)
}

func solveDay09(cmd *cobra.Command, args []string) error {
	machine_config, err := os.ReadFile(args[0])
	if err != nil {
		return err
	}

	// split the config by line
	lines := strings.Split(string(machine_config), "\n")

	MirageMaintenanceSolution := 0
	if runPartTwo {
		MirageMaintenanceSolution = day09.GetMiragemaintenancePart2(lines)
	} else {
		MirageMaintenanceSolution = day09.GetMiragemaintenancePart1(lines)
	}

	fmt.Printf("MirageMaintenanceSolution: %v\n", MirageMaintenanceSolution)
	return nil
}