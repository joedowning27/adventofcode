package main

import (
	"fmt"
	"os"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/joedowning27/adventofcode/day10"
)

var (
	day10Cmd = &cobra.Command{
		Use:   "day10 [input_file]",
		Args:  cobra.ExactArgs(1),
		Short: "Run the solution for day 10",
		RunE:  solveDay10,
	}
)

func init() {
	day10Cmd.Flags().BoolVarP(&runPartTwo, "part2", "p", false, "run part two")
	solveCmd.AddCommand(day10Cmd)
}

func solveDay10(cmd *cobra.Command, args []string) error {
	machine_config, err := os.ReadFile(args[0])
	if err != nil {
		return err
	}

	// split the config by line
	lines := strings.Split(string(machine_config), "\n")

	PipeMazeSolution := 0
	if runPartTwo {
		PipeMazeSolution = day10.GetPipemazePart2(lines)
	} else {
		PipeMazeSolution = day10.GetPipemazePart1(lines)
	}

	fmt.Printf("PipeMazeSolution: %v\n", PipeMazeSolution)
	return nil
}