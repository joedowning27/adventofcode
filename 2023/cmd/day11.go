package main

import (
	"fmt"
	"os"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/joedowning27/adventofcode/day11"
)

var (
	day11Cmd = &cobra.Command{
		Use:   "day11 [input_file]",
		Args:  cobra.ExactArgs(1),
		Short: "Run the solution for day 11",
		RunE:  solveDay11,
	}
)

func init() {
	day11Cmd.Flags().BoolVarP(&runPartTwo, "part2", "p", false, "run part two")
	solveCmd.AddCommand(day11Cmd)
}

func solveDay11(cmd *cobra.Command, args []string) error {
	input, err := os.ReadFile(args[0])
	if err != nil {
		return err
	}

	// split the config by line
	lines := strings.Split(string(input), "\n")

	// if the last line is empty ignore it
	if len(lines[len(lines)-1]) <= 0 {
		lines = lines[:len(lines)-1]
	}

	CosmicExpansionSolution := 0
	if runPartTwo {
		CosmicExpansionSolution = day11.GetCosmicExpansionPart2(lines)
	} else {
		CosmicExpansionSolution = day11.GetCosmicExpansionPart1(lines)
	}

	fmt.Printf("CosmicExpansion Solution: %v\n", CosmicExpansionSolution)
	return nil
}
