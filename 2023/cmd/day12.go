package main

import (
	"fmt"
	"os"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/joedowning27/adventofcode/day12"
)

var (
	day12Cmd = &cobra.Command{
		Use:   "day12 [input_file]",
		Args:  cobra.ExactArgs(1),
		Short: "Run the solution for day 12",
		RunE:  solveDay12,
	}
)

func init() {
	day12Cmd.Flags().BoolVarP(&runPartTwo, "part2", "p", false, "run part two")
	solveCmd.AddCommand(day12Cmd)
}

func solveDay12(cmd *cobra.Command, args []string) error {
	input, err := os.ReadFile(args[0])
	if err != nil {
		return err
	}

	// split the input by line
	lines := strings.Split(string(input), "\n")

	// if the last line is empty ignore it
	if len(lines[len(lines)-1]) <= 0 {
		lines = lines[:len(lines)-1]
	}

	HotSpringsSolution := 0
	if runPartTwo {
		HotSpringsSolution = day12.GetHotSpringsPart2(lines)
	} else {
		HotSpringsSolution = day12.GetHotSpringsPart1(lines)
	}

	fmt.Printf("HotSprings Solution: %v\n", HotSpringsSolution)
	return nil
}