package main

import (
	"errors"
	"fmt"
	"io"
	"io/fs"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"text/template"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var rootCmd = &cobra.Command{
	Use:   "adventofcode [OPTIONS] [COMMANDS]",
	Short: "Advent of Code 2023 CLI",
}

var solveCmd = &cobra.Command{
	Use:   "solve [COMMAND]",
	Short: "Select a day to solve",
}

var startCmd = &cobra.Command{
	Use:   "start [Day Number] [Title]",
	Short: "Start a new day",
	Args:  cobra.ExactArgs(2),
	Run:   startDay,
}

var submitCmd = &cobra.Command{
	Use:   "submit [Day Number]",
	Short: "Submit a day to AOC website",
	Args:  cobra.ExactArgs(1),
	Run:   submitDay,
}

var fetchCmd = &cobra.Command{
	Use:   "fetch [Day Number]",
	Short: "Fetch the day's input from the AOC website",
	Args:  cobra.ExactArgs(1),
	Run:   fetchDay,
}

var runPartTwo = false

func main() {
	// XXX I would like a command that will submit my result
	// https://mmhaskell.com/blog/2023/1/30/advent-of-code-fetching-puzzle-input-using-the-api
	// 	[ ] fetch problem from advent of code website
	// 		[ ] parse html into markdown
	// 	[x] fetch input from advent of code website
	//	[ ] run that days solution on the input
	//	[ ]	submit the result to advent of code
	err := viper.ReadInConfig() // Find and read the config file
	if err != nil {             // Handle errors reading the config file
		panic(fmt.Errorf("fatal error config file: %w", err))
	}

	rootCmd.Execute()
}

// our name should be in the form FunctionName
// this is to help test cases and to look nice
// if the user gives a lowercase string we can
// correct it: test -> Test
func funcCase(in string) string {
	return strings.ToUpper(in[:1]) + in[1:]
}

func startDay(cmd *cobra.Command, args []string) {
	fmt.Println("Starting day", args[0])
	day, err := strconv.Atoi(args[0])
	if err != nil {
		panic(err)
	}

	data := struct {
		Day  int
		Name string
	}{
		Day:  day,
		Name: args[1],
	}

	funcMap := map[string]any{
		"FuncCase": funcCase,
	}
	// create a directory for dayXX
	dayDir := fmt.Sprintf("day%02d", data.Day)
	err = os.Mkdir(dayDir, 0755)
	if err != nil && !errors.Is(err, fs.ErrExist) {
		panic(err)
	}

	// load templates from template directory
	tmpls := template.New("templates")
	tmpls.Funcs(funcMap)
	template.Must(tmpls.ParseGlob("templates/*.tmpl"))

	// XXX can we parse these and iterate over them in the next part?
	// it would be nice to add templates automatically
	fmt.Println(tmpls.DefinedTemplates())

	//execute templates
	makeFromTemplate(fmt.Sprintf("%s/%s.go", dayDir, data.Name), "code.go.tmpl", tmpls, data)
	makeFromTemplate(fmt.Sprintf("%s/%s_test.go", dayDir, data.Name), "code_test.go.tmpl", tmpls, data)
	makeFromTemplate(fmt.Sprintf("cmd/day%02d.go", data.Day), "dayCmd.go.tmpl", tmpls, data)
}

func makeFromTemplate(dest string, name string, tmpl *template.Template, data any) {
	cmdFile, err := os.Create(dest)
	if err != nil {
		panic(err)
	}
	defer cmdFile.Close()
	err = tmpl.ExecuteTemplate(cmdFile, name, data)
	if err != nil {
		panic(err)
	}
}

func submitDay(cmd *cobra.Command, args []string) {
	fmt.Println("Submitting day", args[0])
	// run our solution for day x and store the answer

	// post answer
	// POST https://adventofcode.com/2023/day/7/answer
	// BODY level=1&answer=1234

	// Parse response
	// we will need some way of parsing this. A library like beautiful soup could help us scrape this response
	// ...
	// <main>
	// 	<article><p>That's not the right answer; your answer is too low.  If you're stuck, make sure you're using the full input data; there are also some general tips on the <a href="/2023/about">about page</a>, or you can ask for hints on the <a href="https://www.reddit.com/r/adventofcode/" target="_blank">subreddit</a>.  Please wait one minute before trying again. <a href="/2023/day/7">[Return to Day 7]</a></p></article>
	// </main>
	// ...

	// Display scraped response to user, maybe throw in some colors
}

func fetchDay(cmd *cobra.Command, args []string) {
	fmt.Printf("Fetching Input for day %v\n", args[0])
	// check input directory to see if day was already fetched
	inputDir := viper.GetString("inputDir")
	pathStr := filepath.Join(inputDir, args[0]+".txt")
	if info, err := os.Stat(pathStr); err == nil {
		// if the day already exists no need to fetch it
		fmt.Printf("Found %s!\n", info.Name())
		return
	}
	day, err := strconv.Atoi(args[0])
	if err != nil {
		log.Fatal("Day argument must be an integer")
	}

	fetchInput(viper.GetInt("year"), day, pathStr)
}

func fetchInput(year int, day int, path string) {
	var err error
	var res *http.Response
	client := &http.Client{}

	// set up our GET request
	inputURL := fmt.Sprintf("https://adventofcode.com/%d/day/%d/input", year, day)
	req, err := http.NewRequest("GET", inputURL, nil)
	if err != nil {
		log.Fatalf("Failed to generate request %\n", err)
	}
	req.Header.Add("Cookie", viper.GetString("aocToken"))

	// execute the request
	if res, err = client.Do(req); err != nil {
		log.Fatalf("HTTP error: %v", err)
	}
	defer res.Body.Close()

	// check response for errors
	if res.StatusCode != 200 {
		log.Fatalf("Request Failed with status: %s\n", res.Status)
	}
	body, err := io.ReadAll(res.Body)
	if err != nil {
		log.Fatalf("Failed to read request: %v\n", err)
	}
	// write the input to our file
	f, err := os.Create(path)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	f.Write(body)
	fmt.Printf("Input for Day %d Fetched! Good Luck!", day)
}

func init() {
	viper.SetConfigName("config")              // name of config file (without extension)
	viper.SetConfigType("toml")                // REQUIRED if the config file does not have the extension in the name
	viper.AddConfigPath("/etc/adventofcode/")  // path to look for the config file in
	viper.AddConfigPath("$HOME/.adventofcode") // call multiple times to add many search paths
	viper.AddConfigPath(".")                   // optionally look for config in the working directory

	rootCmd.AddCommand(solveCmd)
	rootCmd.AddCommand(startCmd)
	rootCmd.AddCommand(submitCmd)
	rootCmd.AddCommand(fetchCmd)
}
