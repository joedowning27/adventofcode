package day01

import (
	"fmt"
	"strconv"
	"strings"
)

func GetCalibrationTotal(in []string, stringNums bool) int {
	total := 0
	for _, v := range in {
		val := GetCalibration(v, stringNums)
		// fmt.Printf("%s -> %d\n", v, val)
		total += val
	}
	return total
}

// the calibration value can be found by combining the
// first digit and the last digit (in that order) to form
// a single two-digit number.

func GetCalibration(in string, checkWords bool) int {
	digits := "1234567890"
	digitStrs := []string{
		"zero", "one", "two", "three", "four",
		"five", "six", "seven", "eight", "nine",
	}

	var fidx = len(in)
	var lidx = -1
	var fval, lval string

	// if there is a digit in the input we can take it's index
	if strings.ContainsAny(in, digits) {
		fidx = strings.IndexAny(in, digits)
		fval = string(in[fidx])

		lidx = strings.LastIndexAny(in, digits)
		lval = string(in[lidx])
	}

	if checkWords {
		for i, digit := range digitStrs {
			if idx := strings.Index(in, digit); idx != -1 && idx < fidx {
				fidx = idx
				fval = fmt.Sprint(i)
			}
			if idx := strings.LastIndex(in, digit); idx != -1 && idx > lidx {
				lidx = idx
				lval = fmt.Sprint(i)
			}
		}
	}

	// append the two digits
	total, _ := strconv.ParseInt(fval+lval, 10, 32)
	return int(total)
}
