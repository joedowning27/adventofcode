package day01

import (
	"reflect"
	"testing"
)

// TODO can we somehow convert this to a macro? I would like to use CATCH2 style macros
func Check(t *testing.T, actual, expected any) {
	// fmt.Printf("Comparing %v == %v\n", expected, actual)
	if !reflect.DeepEqual(expected, actual) {
		// XXX This string is ugly but it works, gotta fix this string literal later
		t.Errorf(
			`Check failed:
Actual: "%d"
Expected: "%d"`, actual, expected)
	}
}

func TestGetCalibrationTotal(t *testing.T) {
	in := []string{
		"1abc2",
		"pqr3stu8vwx",
		"a1b2c3d4e5f",
		"treb7uchet",
	}
	out := GetCalibrationTotal(in, false)
	Check(t, out, 142)
}

func TestGetCalibrationTotalStr(t *testing.T) {
	in := []string{
		"two1nine",
		"eightwothree",
		"abcone2threexyz",
		"xtwone3four",
		"4nineeightseven2",
		"zoneight234",
		"7pqrstsixteen",
	}
	expected := 281
	out := GetCalibrationTotal(in, true)
	Check(t, out, expected)
}
func TestGetCalibration(t *testing.T) {
	in := []string{
		"1abc2",
		"pqr3stu8vwx",
		"a1b2c3d4e5f",
		"treb7uchet",
		"fx3",
	}
	expected := []int{12, 38, 15, 77, 33}
	for i, v := range in {
		out := GetCalibration(v, false)
		Check(t, out, expected[i])
	}
}

func TestGetCalibrationStr(t *testing.T) {
	in := []string{
		"two1nine",
		"eightwothree",
		"abcone2threexyz",
		"xtwone3four",
		"4nineeightseven2",
		"zoneight234",
		"7pqrstsixteen",
	}
	expected := []int{29, 83, 13, 24, 42, 14, 76}
	for i, v := range in {
		out := GetCalibration(v, true)
		Check(t, out, expected[i])
	}
}

func TestGetCalibrationEdge(t *testing.T) {
	in := []string{
		"fx3",
		"1hrqthmr",
		"sevenine",
		"eightwothree",
		"1eightwo",
		"twone",
		"fives",
		"pfives",
		"pfive",
		"eightwo",
		"eight64925eightffnlprnnfkpggkkl",
		"2rzvpfpgzxk3863eightoneighttbb",
	}
	expected := []int{33, 11, 79, 83, 12, 21, 55, 55, 55, 82, 88, 28}
	for i, v := range in {
		out := GetCalibration(v, true)
		Check(t, out, expected[i])
	}
}
