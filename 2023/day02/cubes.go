package day02

import (
	"regexp"
	"strconv"
)

func GetCubeGameTotal(in []string, bag map[string]int64) int64 {
	var total int64 = 0
	for _, v := range in {
		total += GetCubeGame(v, bag)
	}
	return total
}

func GetCubeGame(in string, bag map[string]int64) int64 {
	gameRE := regexp.MustCompile(`Game (\d+):(.*)`)
	handRE := regexp.MustCompile(`(\d+) (red|green|blue)`)

	// get the Game number
	matches := gameRE.FindStringSubmatch(in)
	gameNum, _ := strconv.ParseInt(matches[1], 10, 32)

	// parse hands
	diceCounts := handRE.FindAllStringSubmatch(matches[2], -1)
	// if count > bag value return 0
	for _, count := range diceCounts {
		countVal, _ := strconv.ParseInt(count[1], 10, 32)
		if countVal > int64(bag[count[2]]) {
			return 0
		}
	}

	// else return the game number
	return gameNum
}

func GetCubePowerTotal(in []string) int64 {
	var total int64 = 0
	for _, v := range in {
		total += GetCubePower(v)
	}
	return total
}

func GetCubePower(in string) int64 {
	gameRE := regexp.MustCompile(`Game (\d+):(.*)`)
	handRE := regexp.MustCompile(`(\d+) (red|green|blue)`)

	// parse the input, here we only need the hand data
	matches := gameRE.FindStringSubmatch(in)

	// find the minimum set of dice needed to run this game
	minSet := map[string]int64{}
	diceCounts := handRE.FindAllStringSubmatch(matches[2], -1)
	for _, count := range diceCounts {
		countVal, _ := strconv.ParseInt(count[1], 10, 32)
		colorStr := count[2]
		if countVal > minSet[colorStr] {
			minSet[colorStr] = countVal
		}
	}

	// else return the sets power
	return minSet["red"] * minSet["green"] * minSet["blue"]
}
