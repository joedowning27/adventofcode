package day02

import (
	"reflect"
	"testing"
)

func Check(t *testing.T, actual, expected any) {
	// fmt.Printf("Comparing %v == %v\n", expected, actual)
	if !reflect.DeepEqual(expected, actual) {
		t.Errorf(
			`Check failed:
Actual: "%d"
Expected: "%d"`, actual, expected)
	}
}

var in = []string{
	"Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green",
	"Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue",
	"Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red",
	"Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red",
	"Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green",
}
var expectedP1 = []int64{1, 2, 0, 0, 5}
var expectedP2 = []int64{48, 12, 1560, 630, 36}
var bag = map[string]int64{
	"red":   12,
	"green": 13,
	"blue":  14,
}

func TestCubeGameTotal(t *testing.T) {
	out := GetCubeGameTotal(in, bag)
	Check(t, out, int64(8))
}

func TestCubeGame(t *testing.T) {
	for i, v := range in {
		out := GetCubeGame(v, bag)
		Check(t, out, expectedP1[i])
	}
}

func TestCubePowerTotal(t *testing.T) {
	out := GetCubePowerTotal(in)
	Check(t, out, int64(2286))
}

func TestCubePower(t *testing.T) {
	for i, v := range in {
		out := GetCubePower(v)
		Check(t, out, expectedP2[i])
	}
}
