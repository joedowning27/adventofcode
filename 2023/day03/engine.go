package day03

import (
	"regexp"
	"strconv"
	"strings"
)

type point struct {
	x, y int
}
type numpoint struct {
	x, y int
	len  int
	val  int
}

func GetSerialTotal(in []string) int {
	total := 0
	// find all the number in the numbers
	numPoints := getNumberPoints(in)
	for _, np := range numPoints {
		// calculate the adjacent points
		neighbors := getNeighbors(in, np)
		// check if any of the points are symbols
		for _, n := range neighbors {
			// if a symbol is present then add it to the total
			if strings.Contains("*+=&$-%#@/", string(in[n.y][n.x])) {
				total += np.val
				break
			}
		}
	}
	return total
}

func GetGearsTotal(in []string) int {
	// find and sum up all the gear ratios
	// a gear is a * surrounded by exactly 2 numbers
	// find all number points
	gearMap := make(map[point][]numpoint)
	numPoints := getNumberPoints(in)
	for _, np := range numPoints {
		// get the neighbors
		neighbors := getNeighbors(in, np)
		// check if any surrounding symbols are gears
		for _, n := range neighbors {
			// if they are gears add them to the "found gears map"
			if string(in[n.y][n.x]) == "*" {
				gearMap[n] = append(gearMap[n], np)
			}
		}
	}
	// iterate through the map and calculate gear ratios
	total := 0
	for _, nums := range gearMap {
		if len(nums) == 2 {
			ratio := nums[0].val * nums[1].val
			total += ratio
		}
	}
	return total
}

func getNumberPoints(in []string) []numpoint {
	points := []numpoint{}
	// loop through every row
	re := regexp.MustCompile(`\d+`)
	for y, row := range in {
		// find all digits using regex.FindAllIndex
		found := re.FindAllIndex([]byte(row), -1)
		// convert each of the found digits into a numpoint
		for _, v := range found {
			if val, err := strconv.Atoi(row[v[0]:v[1]]); err == nil {
				points = append(points, numpoint{
					x:   v[0],
					y:   y,
					len: v[1] - v[0],
					val: val,
				})
			}

		}
	}
	return points
}
func getNeighbors(in []string, np numpoint) []point {
	// we need to return the following points
	// {x-1, y-1}, 	{x, y-1}, 	...,	{x+np.len, y-1}
	// {x-1, y}, 		np0, 	...,	{x+np.len, y}
	// {x-1, y+1}, 	{x, y+1},	...,	{x+np.len, y+1}
	points := []point{}
	for y := np.y - 1; y <= np.y+1; y++ {
		for x := np.x - 1; x <= np.x+np.len; x++ {
			// if the "neighbor" is out of range continue
			if (y < 0 || y >= len(in)) || (x < 0 || x >= len(in[y])) {
				continue
			}
			// if the "neighbor" is in our number continue
			if y == np.y && x >= np.x && x < np.x+np.len {
				continue
			}
			points = append(points, point{x, y})
		}
	}
	return points
}
