package day03

import (
	"reflect"
	"testing"
)

func Check(t *testing.T, actual, expected any) {
	// fmt.Printf("Comparing %v == %v\n", expected, actual)
	if !reflect.DeepEqual(expected, actual) {
		t.Errorf(
			`Check failed:
Actual: "%d"
Expected: "%d"`, actual, expected)
	}
}

var in = []string{
	"467..114..",
	"...*......",
	"..35..633.",
	"......#...",
	"617*......",
	".....+.58.",
	"..592.....",
	"......755.",
	"...$.*....",
	".664.598..",
}

func TestSerialTotal(t *testing.T) {
	out := GetSerialTotal(in)
	Check(t, out, 4361)
}

func TestGearsTotal(t *testing.T) {
	out := GetGearsTotal(in)
	Check(t, out, 467835)
}
func TestSerialOverlap(t *testing.T) {
	var inOverlap = []string{
		"...$.*....",
		".664.598..",
		"..*.......",
	}
	out := GetSerialTotal(inOverlap)
	Check(t, out, 1262)
}

func TestAllDirections(t *testing.T) {
	pattern := []string{
		".....",
		".111.",
		".....",
	}
	out := GetSerialTotal(pattern)
	Check(t, out, 0)
	pattern = []string{
		"*....",
		".111.",
		".....",
	}
	out = GetSerialTotal(pattern)
	Check(t, out, 111)
	pattern = []string{
		"..*..",
		".222.",
		".....",
	}
	out = GetSerialTotal(pattern)
	Check(t, out, 222)
	pattern = []string{
		"....*",
		".333.",
		".....",
	}
	out = GetSerialTotal(pattern)
	Check(t, out, 333)
	pattern = []string{
		".....",
		"*444.",
		".....",
	}
	out = GetSerialTotal(pattern)
	Check(t, out, 444)
	pattern = []string{
		".....",
		".555*",
		".....",
	}
	out = GetSerialTotal(pattern)
	Check(t, out, 555)
	pattern = []string{
		".....",
		".666.",
		"*....",
	}
	out = GetSerialTotal(pattern)
	Check(t, out, 666)
	pattern = []string{
		".....",
		".777.",
		"..*..",
	}
	out = GetSerialTotal(pattern)
	Check(t, out, 777)
	pattern = []string{
		".....",
		".888.",
		"....*",
	}
	out = GetSerialTotal(pattern)
	Check(t, out, 888)
}

func TestGetNumberPoints(t *testing.T) {
	numStrs := []string{
		"123..456.78",
		".34.5765..1",
	}
	expected := []numpoint{
		{x: 0, y: 0, len: 3, val: 123},
		{x: 5, y: 0, len: 3, val: 456},
		{x: 9, y: 0, len: 2, val: 78},
		{x: 1, y: 1, len: 2, val: 34},
		{x: 4, y: 1, len: 4, val: 5765},
		{x: 10, y: 1, len: 1, val: 1},
	}
	out := getNumberPoints(numStrs)
	Check(t, out, expected)
}

func TestGetNeighbors(t *testing.T) {
	smallGrid := []string{
		".12..",
		"56.78",
		"..34.",
	}
	up := numpoint{x: 1, y: 0, len: 2, val: 12}
	expUp := []point{
		// no above
		{0, 0}, {3, 0},
		{0, 1}, {1, 1}, {2, 1}, {3, 1},
	}
	outUp := getNeighbors(smallGrid, up)
	Check(t, outUp, expUp)

	down := numpoint{x: 2, y: 2, len: 2, val: 34}
	expDown := []point{
		{1, 1}, {2, 1}, {3, 1}, {4, 1},
		{1, 2}, {4, 2},
		// no below
	}
	outDown := getNeighbors(smallGrid, down)
	Check(t, outDown, expDown)

	left := numpoint{x: 0, y: 1, len: 2, val: 56}
	expLeft := []point{
		{0, 0}, {1, 0}, {2, 0},
		{2, 1}, // no left side
		{0, 2}, {1, 2}, {2, 2},
	}
	outLeft := getNeighbors(smallGrid, left)
	Check(t, outLeft, expLeft)

	right := numpoint{x: 3, y: 1, len: 2, val: 78}
	expRight := []point{
		{2, 0}, {3, 0}, {4, 0},
		{2, 1}, // no right side
		{2, 2}, {3, 2}, {4, 2},
	}
	outRight := getNeighbors(smallGrid, right)
	Check(t, outRight, expRight)
}
