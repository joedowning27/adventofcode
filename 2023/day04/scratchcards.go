package day04

import (
	"fmt"
	"math"
	"regexp"
)

type Card struct {
	winners []string
	numbers []string
	winning int
	count   int
}

func GetScratchcardsPart1(in []string) int {
	cards := GetCards(in)
	return GetTotalScore(cards)
}

func GetScratchcardsPart2(in []string) int {
	cards := GetCards(in)
	for i, card := range cards {
		cards[i].winning = GetWinnerMatches(card)
		for w := 1; w <= cards[i].winning && w+i < len(cards); w++ {
			cards[i+w].count += cards[i].count
		}
	}
	total := 0
	for _, card := range cards {
		total += card.count
	}
	return total
}

func GetCards(in []string) []Card {
	cards := []Card{}
	for _, line := range in {
		if line != "" {
			cards = append(cards, GetCard(line))
		}
	}
	return cards
}

func GetCard(in string) Card {
	card := Card{nil, nil, 0, 1}
	cardRE := regexp.MustCompile(`Card *\d+: ([\d ]+) \|+ ([\d ]+)`)
	matches := cardRE.FindStringSubmatch(in)
	if len(matches) > 0 {
		digitRE := regexp.MustCompile(`\d+`)
		card.winners = digitRE.FindAllString(matches[1], -1)
		card.numbers = digitRE.FindAllString(matches[2], -1)
	} else {
		panic(fmt.Sprintf("Failed to parse card: '%v'", in))
	}
	return card
}

func GetTotalScore(cards []Card) int {
	total := 0
	for c := range cards {
		total += GetCardScore(cards[c])
	}
	return total
}

func GetWinnerMatches(card Card) int {
	// find the total number of winners in numbers
	numWinners := 0
	for _, winner := range card.winners {
		for _, number := range card.numbers {
			if winner == number {
				numWinners++
			}
		}
	}
	return numWinners
}

func GetCardScore(card Card) int {
	numWinners := GetWinnerMatches(card)
	if numWinners == 0 {
		return 0
	}
	// score = 2 ^ (numWinners - 1)
	score := math.Pow(2, float64(numWinners-1))
	return int(score)
}
