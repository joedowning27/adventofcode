package day04

import (
	"reflect"
	"testing"
)

func Check(t *testing.T, actual, expected any) {
	// fmt.Printf("Comparing %v == %v\n", expected, actual)
	if !reflect.DeepEqual(expected, actual) {
		t.Errorf(
			`Check failed:
Actual: "%d"
Expected: "%d"`, actual, expected)
	}
}

var in = []string{
	"Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53",
	"Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19",
	"Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1",
	"Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83",
	"Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36",
	"Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11",
}

var expectedCards = []Card{
	{[]string{"41", "48", "83", "86", "17"}, []string{"83", "86", "6", "31", "17", "9", "48", "53"}, 0, 1},
	{[]string{"13", "32", "20", "16", "61"}, []string{"61", "30", "68", "82", "17", "32", "24", "19"}, 0, 1},
	{[]string{"1", "21", "53", "59", "44"}, []string{"69", "82", "63", "72", "16", "21", "14", "1"}, 0, 1},
	{[]string{"41", "92", "73", "84", "69"}, []string{"59", "84", "76", "51", "58", "5", "54", "83"}, 0, 1},
	{[]string{"87", "83", "26", "28", "32"}, []string{"88", "30", "70", "12", "93", "22", "82", "36"}, 0, 1},
	{[]string{"31", "18", "13", "56", "72"}, []string{"74", "77", "10", "23", "35", "67", "36", "11"}, 0, 1},
}

func TestScratchcardsPart1(t *testing.T) {
	out := GetScratchcardsPart1(in)
	Check(t, out, 13)
}

func TestScratchcardsPart2(t *testing.T) {
	out := GetScratchcardsPart2(in)
	Check(t, out, 30)
}

func TestGetCards(t *testing.T) {
	out := GetCards(in)
	Check(t, out, expectedCards)
}

func TestGetCard(t *testing.T) {
	out := GetCard(in[0])
	Check(t, out, expectedCards[0])
}

func TestGetCardScore(t *testing.T) {
	Check(t, GetCardScore(expectedCards[0]), 8)
	Check(t, GetCardScore(expectedCards[1]), 2)
	Check(t, GetCardScore(expectedCards[2]), 2)
	Check(t, GetCardScore(expectedCards[3]), 1)
	Check(t, GetCardScore(expectedCards[4]), 0)
	Check(t, GetCardScore(expectedCards[5]), 0)
}
