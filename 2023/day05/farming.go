package day05

import (
	"fmt"
	"regexp"
	"strconv"
)

type rangeMap struct {
	srcType string
	dstType string
	spans   []span
}
type span struct {
	srcStart int
	dstStart int
	len      int
}

// XXX this could be rewritten to use ranges and consolidate code
func GetFarmingPart1(in []string) int {
	// parse input into seeds and maps
	seeds := GetSeeds(in[0])
	maps := GetRangeMaps(in[2:])

	// apply all the maps to each seed
	locations := []int{}
	for _, seed := range seeds {
		for _, m := range maps {
			seed = ApplyMap(seed, m)
		}
		locations = append(locations, seed)
	}
	// find the minimum location
	min := locations[0]
	for _, l := range locations {
		if l < min {
			min = l
		}
	}
	return min
}

type seedRange struct {
	start int
	len   int
}

func GetFarmingPart2(in []string) int {
	// parse input into seed ranges and maps
	seedRanges := GetSeedRanges(in[0])
	maps := GetRangeMaps(in[2:])
	// apply the maps to each range
	locations := []seedRange{}
	for _, r := range seedRanges {
		rs := []seedRange{r}
		for _, m := range maps {
			rs = ApplyMapToRanges(rs, m)
		}
		locations = append(locations, rs...)
	}
	// find the minimum location number from the result
	min := locations[0].start
	for _, l := range locations {
		if l.start < min {
			min = l.start
		}
	}
	return min
}

func GetSeeds(in string) []int {
	seedRE := regexp.MustCompile(`\d+`)
	seedStrs := seedRE.FindAllString(in, -1)
	seeds := []int{}
	for _, str := range seedStrs {
		seed, err := strconv.Atoi(str)
		if err != nil {
			panic(err)
		}
		seeds = append(seeds, seed)
	}
	return seeds
}

func GetSeedRanges(in string) []seedRange {
	seeds := GetSeeds(in)
	ranges := []seedRange{}
	for i := 0; i < len(seeds)-1; i += 2 {
		ranges = append(ranges, seedRange{seeds[i], seeds[i+1]})
	}
	return ranges
}

func GetRangeMap(in []string) rangeMap {
	mapRE := regexp.MustCompile(`(.*)-to-(.*) map:`)
	matches := mapRE.FindStringSubmatch(in[0])
	m := rangeMap{}
	if len(matches) > 0 {
		m.srcType = matches[1]
		m.dstType = matches[2]
	}
	for _, line := range in[1:] {
		digitRE := regexp.MustCompile(`\d+`)
		digitStrs := digitRE.FindAllString(line, -1)
		if len(digitStrs) != 3 {
			panic(fmt.Sprint("found a line that didn't have 3 digits\n", line))
		}
		dstStart, err := strconv.Atoi(digitStrs[0])
		if err != nil {
			panic(err)
		}
		srcStart, err := strconv.Atoi(digitStrs[1])
		if err != nil {
			panic(err)
		}
		len, err := strconv.Atoi(digitStrs[2])
		if err != nil {
			panic(err)
		}
		m.spans = append(m.spans, span{srcStart, dstStart, len})
	}

	return m
}

func GetRangeMaps(in []string) []rangeMap {
	rangeMaps := []rangeMap{}
	//maps are separated by empty strings
	startIdx := 0
	for i := 1; i < len(in); i++ {
		if in[i] == "" {
			m := GetRangeMap(in[startIdx:i])
			rangeMaps = append(rangeMaps, m)
			startIdx = i + 1
			i++ // skip past the empty line
		}
	}
	// if the last line is not empty we should still
	// get the last map
	final := in[startIdx:]
	if len(final) > 0 {
		m := GetRangeMap(in[startIdx:])
		rangeMaps = append(rangeMaps, m)
	}
	return rangeMaps
}

func ApplyMap(seed int, m rangeMap) int {
	for _, span := range m.spans {
		if seed >= span.srcStart && seed < span.srcStart+span.len {
			return span.dstStart + (seed - span.srcStart)
		}
	}
	return seed
}

func ApplyMapToRanges(seeds []seedRange, m rangeMap) []seedRange {
	res := []seedRange{}
	for _, s := range seeds {
		res = append(res, ApplyMapToRange(s, m)...)
	}
	return res
}

func ApplyMapToRange(seed seedRange, m rangeMap) []seedRange {
	for _, span := range m.spans {
		// if the start falls in this range we need to apply this span
		if seed.start >= span.srcStart && seed.start < span.srcStart+span.len {
			// if the seed range is longer than the span it needs to be split
			if seed.start+seed.len > span.srcStart+span.len {
				partialLen := span.len - (seed.start - span.srcStart)
				newRange := seedRange{span.dstStart + (seed.start - span.srcStart), partialLen}
				remainingRange := seedRange{span.srcStart + span.len, seed.len - partialLen}
				return append([]seedRange{newRange}, ApplyMapToRange(remainingRange, m)...)
			}
			// otherwise we can map this range with the current span
			newRange := seedRange{span.dstStart + (seed.start - span.srcStart), seed.len}
			return []seedRange{newRange}
		}
	}
	// if no spans match then we can return the same range
	return []seedRange{seed}
}
