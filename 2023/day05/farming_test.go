package day05

import (
	"reflect"
	"testing"
)

func Check(t *testing.T, actual, expected any) {
	// fmt.Printf("Comparing %v == %v\n", expected, actual)
	if !reflect.DeepEqual(expected, actual) {
		t.Errorf(
			`Check failed:
Actual: "%d"
Expected: "%d"`, actual, expected)
	}
}

var example = []string{
	"seeds: 79 14 55 13",
	"",
	"seed-to-soil map:",
	"50 98 2",
	"52 50 48",
	"",
	"soil-to-fertilizer map:",
	"0 15 37",
	"37 52 2",
	"39 0 15",
	"",
	"fertilizer-to-water map:",
	"49 53 8",
	"0 11 42",
	"42 0 7",
	"57 7 4",
	"",
	"water-to-light map:",
	"88 18 7",
	"18 25 70",
	"",
	"light-to-temperature map:",
	"45 77 23",
	"81 45 19",
	"68 64 13",
	"",
	"temperature-to-humidity map:",
	"0 69 1",
	"1 0 69",
	"",
	"humidity-to-location map:",
	"60 56 37",
	"56 93 4",
}

var expectedRangeMaps = []rangeMap{
	{"seed", "soil", []span{{98, 50, 2}, {50, 52, 48}}},
	{"soil", "fertilizer", []span{{15, 0, 37}, {52, 37, 2}, {0, 39, 15}}},
}

func TestFarmingPart1(t *testing.T) {
	out := GetFarmingPart1(example)
	Check(t, out, 35)
}

func TestFarmingPart2(t *testing.T) {
	out := GetFarmingPart2(example)
	Check(t, out, 46)
}

func TestGetSeeds(t *testing.T) {
	seedStr := "seeds: 79 14 55 13"
	out := GetSeeds(seedStr)
	Check(t, out, []int{79, 14, 55, 13})
}

func TestGetSeedRanges(t *testing.T) {
	seedStr := "seeds: 79 14 55 13"
	out := GetSeedRanges(seedStr)
	Check(t, out, []seedRange{{79, 14}, {55, 13}})
}

func TestGetRangeMap(t *testing.T) {
	seedToSoilMapStr := example[2:5]
	out := GetRangeMap(seedToSoilMapStr)
	Check(t, out, expectedRangeMaps[0])
}

func TestGetRangeMaps(t *testing.T) {
	testMapStrs := example[2:10]
	out := GetRangeMaps(testMapStrs)
	Check(t, out, expectedRangeMaps)
}

func TestApplyMap(t *testing.T) {
	seedToSoilMap := expectedRangeMaps[0]
	Check(t, ApplyMap(98, seedToSoilMap), 50)
	Check(t, ApplyMap(99, seedToSoilMap), 51)

	Check(t, ApplyMap(50, seedToSoilMap), 52)
	Check(t, ApplyMap(51, seedToSoilMap), 53)

	Check(t, ApplyMap(79, seedToSoilMap), 81)
	Check(t, ApplyMap(14, seedToSoilMap), 14)
	Check(t, ApplyMap(55, seedToSoilMap), 57)
	Check(t, ApplyMap(13, seedToSoilMap), 13)
}

func TestApplyMapToRange(t *testing.T) {
	seedToSoilMap := expectedRangeMaps[0]
	r := seedRange{79, 30}
	out := ApplyMapToRange(r, seedToSoilMap)
	Check(t, out, []seedRange{{81, 19}, {50, 2}, {100, 9}})
}
