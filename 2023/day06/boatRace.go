package day06

import (
	"fmt"
	"math"
	"strconv"
	"strings"

	"gitlab.com/joedowning27/adventofcode/utils"
)

func GetBoatRacePart1(in []string) int {
	fmt.Println("Day 6 Part 1")
	// parse the input, first line is times, second is distances separated by spaces
	times := utils.GetInts(in[0])
	dists := utils.GetInts(in[1])
	if len(times) != len(dists) {
		panic("need time and distances entries for each race")
	}
	// calculate the number of ways we could win in each race
	result := 1
	for i := 0; i < len(times); i++ {
		// the final result is all intermediate result multiplied together
		result *= GetWaysToWin(times[i], dists[i])
	}
	return result
}

func GetWaysToWin(time int, dist int) int {
	// find the roots of our distance equation
	// Dist = x^2 - Tx
	// quadratic eq gives us two roots
	x1 := (float64(time) + math.Sqrt(float64(time*time-4*dist))) / 2
	x2 := (float64(time) - math.Sqrt(float64(time*time-4*dist))) / 2

	// all integers in between are wins
	wins := int(math.Floor(x1) - math.Ceil(x2+1))

	// check if either "near root" int would result in a win
	if int(math.Floor(x1))*(time-int(math.Floor(x1))) > dist {
		wins++
	}
	if int(math.Ceil(x2))*(time-int(math.Ceil(x2))) > dist {
		wins++
	}
	return wins
}

func GetBoatRacePart2(in []string) int {
	// the only difference between part 1 and part 2 is how we parse the input
	// each line of the input contains a word followed by one big number with spaces in it
	// input is in the for .+:[/w/d]+
	var time, dist int
	var err error

	timeStr := strings.Split(in[0], ":")[1]
	timeStr = strings.ReplaceAll(timeStr, " ", "")
	if time, err = strconv.Atoi(timeStr); err != nil {
		panic(err)
	}

	distStr := strings.Split(in[1], ":")[1]
	distStr = strings.ReplaceAll(distStr, " ", "")
	if dist, err = strconv.Atoi(distStr); err != nil {
		panic(err)
	}
	return GetWaysToWin(time, dist)
}
