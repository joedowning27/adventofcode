package day06

import (
	"reflect"
	"testing"
)

func Check(t *testing.T, actual, expected any) {
	// fmt.Printf("Comparing %v == %v\n", expected, actual)
	if !reflect.DeepEqual(expected, actual) {
		t.Errorf(
			`Check failed:
Actual: "%d"
Expected: "%d"`, actual, expected)
	}
}

var in = []string{
	"Time:      7  15   30",
	"Distance:  9  40  200",
}

var bigRace = []string{
	"Time:      71530",
	"Distance:  940200",
}

func TestGetBoatRacePart1(t *testing.T) {
	out := GetBoatRacePart1(in)
	Check(t, out, 288)
}

func TestGetBoatRacePart2(t *testing.T) {
	out := GetBoatRacePart1(bigRace)
	Check(t, out, 71503)
}

func TestGetWaysToWin(t *testing.T) {
	times := []int{7, 15, 30}
	dists := []int{9, 40, 200}
	out := GetWaysToWin(times[0], dists[0])
	Check(t, out, 4)
	out = GetWaysToWin(times[1], dists[1])
	Check(t, out, 8)
	out = GetWaysToWin(times[2], dists[2])
	Check(t, out, 9)
}

// Time: 7
// Dist: 9
// D_total = T_waited * (Time - T_waited)
// T_w 	| T_rem	| D_t 	| D_t > Dist
// 0	| 	7	| 0		| False
// 1	| 	6	| 6		| False
// 2	| 	5	| 10	| True
// 3	| 	4	| 12	| True
// 4	| 	3	| 12	| True
// 5	| 	2	| 10	| True
// 6	| 	1	| 6		| False
// 7	| 	0	| 0		| False

// 9 = x * (7 - x)
// 9 = 7x - x^2
// 0 = -x^2 + 7x - 9
// x^2 - 7x + 9 = 0

// quadratic formula
//	a = 1 	is always 1
// 	b = -7 	= -Time
// 		=> -b = Time
// 		=> b^T is Time^2 since negative squared is always positive
// 	c = 9 	= Dist
// x1 = (-b + sqrt(b^2 - 4ac))/2a
// x2 = (-b - sqrt(b^2 - 4ac))/2a

// X1 = (Time + sqrt(Time^2 - 4*Dist))/2
// X2 = (Time - sqrt(Time^2 - 4*Dist))/2

// X1 ~= 5.303
// X2 ~= 1.697
// Wins = math.Ceil(X1) - math.Ceil(X2)
// Wins = 6 - 2 = 4

// T = 15, D = 40
// X1 = (15 + sqrt(15^2 - 4*40))/2 = 11.531
// X2 = (15 - sqrt(15^2 - 4*40))/2 = 3.468
// Wins = math.Ceil(X1) - math.Ceil(X2)
// Wins = 12 - 4 = 8

// T = 30, D = 200
// X1 = (30 + sqrt(30^2 - 4*200))/2 = 20
// X2 = (30 - sqrt(30^2 - 4*200))/2 = 10
// Wins = math.Ceil(X1) - math.Ceil(X2)
// Wins = 20 - 10 = 10

// oops, we've got an error here
// Wins should be the number of integers between X1 and X2
// that is X1 > n > X2

// D_total = T_waited * (Time - T_waited)
// T_w 	| T_rem	| D_t 	| D_t > Dist
// 0	| 30	| 0		| False
// ...	| ...	| ...	| ...
// 9	| 21	| 189	| False
// 10	| 20	| 200	| False
// 11	| 19	| 209	| True
// ...	| ...	| ...	| True
// 19	| 11	| 209	| True
// 20	| 10	| 200	| False
// 21	| 9		| 189	| False
// ...	| ...	| ...	| False

// Wins = Floor(X1) - Ceil(X2 + 1)
// Wins 20 - Ceil(10 + 1) = 20 - 11 = 9

// T = 15, D = 40, W = 8
// X1 = (15 + sqrt(15^2 - 4*40))/2 = 11.531
// X2 = (15 - sqrt(15^2 - 4*40))/2 = 3.468
// Wins = math.Floor(X1) - math.Ceil(X2 + 1)
// Wins 11 - Ceil(3.48 + 1) = 11 - 5 = 6

// T = 7, D = 9, W = 4
// X1 = (7 + sqrt(7^2 - 4*9))/2 = 5.302
// X2 = (7 - sqrt(7^2 - 4*9))/2 = 1.697
// Wins = math.Floor(X1) - math.Ceil(X2 + 1)
// Wins 5 - Ceil(1.697 + 1) = 5 - 3 = 2

// to correct for the "missing" edges we can just check those manually
// if math.Floor(X1) * (T - math.Floor(X1)) > D
// 		wins ++
