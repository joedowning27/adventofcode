package day07

import (
	"cmp"
	"log"
	"slices"
	"strconv"
	"strings"
)

// Hands come in 7 variants each "stronger" than the last
// A hand represented by a 5 char string can only be exactly
// one type
type handRank int
type ranker func(string) handRank

const (
	HighCard handRank = iota
	OnePair
	TwoPair
	ThreeOfAKind
	FullHouse
	FourOfAKind
	FiveOfAKind
)

// Comparing card values as ints will be easier when doing secondary sorting
type cardMaper func(string) []int

var cardMap = map[rune]int{
	'A': 14,
	'K': 13,
	'Q': 12,
	'J': 11,
	'T': 10,
	'9': 9,
	'8': 8,
	'7': 7,
	'6': 6,
	'5': 5,
	'4': 4,
	'3': 3,
	'2': 2,
}

var jokerMap = map[rune]int{
	'A': 14,
	'K': 13,
	'Q': 12,
	'T': 10,
	'9': 9,
	'8': 8,
	'7': 7,
	'6': 6,
	'5': 5,
	'4': 4,
	'3': 3,
	'2': 2,
	'J': 1,
}

type bet struct {
	// str   string
	hand  handRank
	cards []int
	bet   int
}

func GetCamelcardsPart1(in []string) int {
	// parse each line into a slice of bets
	bets := parseBets(in, getHandRank, makeMapper(cardMap))

	// we need to perform two sorts, lets do this is stages
	// first put all our bets into buckets based on the hand rank
	// next we need to sort buckets by
	slices.SortFunc(bets, cmpHands)

	// each bet gets multiplied by the rank it received compared
	// to the other hands. The weakest hand gets a rank of 1 and
	// the strongest hand gets a rank of len(bets)
	acc := 0
	for i, v := range bets {
		// fmt.Printf("%v, %v\n", v.str, v.hand)
		acc += v.bet * (i + 1)
	}
	return acc
}

func GetCamelcardsPart2(in []string) int {
	// parse each line into a slice of bets
	bets := parseBets(in, getJokerRank, makeMapper(jokerMap))

	// we need to perform two sorts, lets do this is stages
	// first put all our bets into buckets based on the hand rank
	// next we need to sort buckets by
	slices.SortFunc(bets, cmpHands)

	// each bet gets multiplied by the rank it received compared
	// to the other hands. The weakest hand gets a rank of 1 and
	// the strongest hand gets a rank of len(bets)
	acc := 0
	for i, v := range bets {
		// fmt.Printf("%v, %v\n", v.str, v.hand)
		acc += v.bet * (i + 1)
	}
	return acc
}

func cmpHands(a, b bet) int {
	// compare by rank first
	if n := cmp.Compare(a.hand, b.hand); n != 0 {
		return n
	}
	// if ranks are equal then we need to check the hands
	// these should be equal lengths,
	// but let's limit i by both lens just to be safe
	for i := 0; i < len(a.cards) && i < len(b.cards); i++ {
		if v := cmp.Compare(a.cards[i], b.cards[i]); v != 0 {
			return v
		}
	}
	return 0
}

func parseBets(in []string, r ranker, m cardMaper) []bet {
	// iterate over each input string
	bets := []bet{}
	for _, betStr := range in {
		// ignore empty bet strings
		if len(betStr) < 1 {
			continue
		}
		// split the string on spaces
		strs := strings.Split(betStr, " ")
		if len(strs) != 2 {
			log.Fatalf("Bets string not formatted properly '%v'\n", betStr)
		}
		// the first part is our hand
		// get the hands rank
		// the second part is out bet, store that for later
		betNum, err := strconv.Atoi(strs[1])
		if err != nil {
			log.Fatalf("Failed to parse bet string: %v\n", err)
		}
		// bets = append(bets, bet{betStr, getHandRank(strs[0]), mapHandToRanks(strs[0]), betNum})
		bets = append(bets, bet{r(strs[0]), m(strs[0]), betNum})
	}
	return bets
}

func makeMapper(m map[rune]int) func(string) []int {
	return func(hand string) []int {
		ranks := []int{}
		for _, v := range hand {
			ranks = append(ranks, m[v])
		}
		return ranks
	}
}

func getHandRank(hand string) handRank {
	cardCounts := map[rune]int{}
	// iterate over the cards in the hand and
	// count the number of times each card appears
	for _, c := range hand {
		cardCounts[c]++
	}
	// iterate over the map keys and keep the two highest counts
	var first, second int
	for _, v := range cardCounts {
		// if this key is higher than first replace first
		if v > first {
			second = first
			first = v
		} else if v > second {
			// otherwise we still need to check second
			second = v
		}
	}
	switch {
	case first == 5:
		return FiveOfAKind
	case first == 4:
		return FourOfAKind
	case first == 3 && second == 2:
		return FullHouse
	case first == 3 && second != 2:
		return ThreeOfAKind
	case first == 2 && second == 2:
		return TwoPair
	case first == 2 && second != 2:
		return OnePair
	default:
		return HighCard
	}
}

func getJokerRank(hand string) handRank {
	cardCounts := map[rune]int{}
	// iterate over the cards in the hand and
	// count the number of times each card appears
	for _, c := range hand {
		cardCounts[c]++
	}
	// iterate over the map keys and keep the two highest counts
	var first, second, joker int
	for k, v := range cardCounts {
		// if this count is for jokers save it for later
		if k == 'J' {
			joker = v
			continue
		}
		// if this key is higher than first replace first
		if v > first {
			second = first
			first = v
		} else if v > second {
			// otherwise we still need to check second
			second = v
		}
	}
	// joker always builds off the strongest hand
	first += joker

	switch {
	case first == 5:
		return FiveOfAKind
	case first == 4:
		return FourOfAKind
	case first == 3 && second == 2:
		return FullHouse
	case first == 3 && second != 2:
		return ThreeOfAKind
	case first == 2 && second == 2:
		return TwoPair
	case first == 2 && second != 2:
		return OnePair
	default:
		return HighCard
	}
}
