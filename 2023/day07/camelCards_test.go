package day07

import (
	"fmt"
	"reflect"
	"testing"
)

func Checkf(actual, expected any) error {
	if !reflect.DeepEqual(expected, actual) {
		return fmt.Errorf("Check failed:\n Actual: \"%d\"\nExpected: \"%d\"\n", actual, expected)
	}
	return nil
}

var in = []string{
	"32T3K 765",
	"T55J5 684",
	"KK677 28",
	"KTJJT 220",
	"QQQJA 483",
}

func TestGetCamelcardsPart1(t *testing.T) {
	out := GetCamelcardsPart1(in)
	if err := Checkf(out, 6440); err != nil {
		t.Error(err)
	}
}

func TestGetCamelcardsPart2(t *testing.T) {
	out := GetCamelcardsPart2(in)
	if err := Checkf(out, 5905); err != nil {
		t.Error(err)
	}
}

func TestParseBets(t *testing.T) {
	expected := []bet{
		{OnePair, []int{3, 2, 10, 3, 13}, 765},
		{ThreeOfAKind, []int{10, 5, 5, 11, 5}, 684},
		{TwoPair, []int{13, 13, 6, 7, 7}, 28},
		{TwoPair, []int{13, 10, 11, 11, 10}, 220},
		{ThreeOfAKind, []int{12, 12, 12, 11, 14}, 483},
	}
	out := parseBets(in, getHandRank, makeMapper(cardMap))
	if err := Checkf(out, expected); err != nil {
		t.Error(err)
	}
}

func TestCmpHands(t *testing.T) {
	b1 := bet{OnePair, []int{3, 2, 10, 3, 13}, 765}
	b2 := bet{ThreeOfAKind, []int{10, 5, 5, 11, 5}, 684}
	b3 := bet{TwoPair, []int{13, 13, 6, 7, 7}, 28}
	b4 := bet{TwoPair, []int{13, 10, 11, 11, 10}, 220}
	b5 := bet{ThreeOfAKind, []int{12, 12, 12, 11, 14}, 483}
	b6 := bet{FourOfAKind, []int{3, 3, 3, 3, 2}, 0}
	b7 := bet{FourOfAKind, []int{2, 14, 14, 14, 14}, 0}

	// One Pair < Three of a Kind
	out := cmpHands(b1, b2)
	if err := Checkf(out, -1); err != nil {
		t.Error(err)
	}
	// Three of a Kind > One Pair
	out = cmpHands(b2, b1)
	if err := Checkf(out, 1); err != nil {
		t.Error(err)
	}
	// Three of a Kind == Three of a Kind
	out = cmpHands(b2, b2)
	if err := Checkf(out, 0); err != nil {
		t.Error(err)
	}
	// 3 of a Kind QQQJA > 3 of a Kind T55J5
	out = cmpHands(b5, b2)
	if err := Checkf(out, 1); err != nil {
		t.Error(err)
	}
	// 2 Pair KK677 > 2 Pair KTJJT
	out = cmpHands(b3, b4)
	if err := Checkf(out, 1); err != nil {
		t.Error(err)
	}
	// 33332 > 2AAAA
	out = cmpHands(b6, b7)
	if err := Checkf(out, 1); err != nil {
		t.Error(err)
	}
}

func TestGetHandRank(t *testing.T) {
	hands := []string{
		"AAAAA",
		"AA8AA",
		"23332",
		"TTT98",
		"23432",
		"A23A4",
		"23456",
	}
	expected := []handRank{
		FiveOfAKind,
		FourOfAKind,
		FullHouse,
		ThreeOfAKind,
		TwoPair,
		OnePair,
		HighCard,
	}
	// Make sure the test is set up properly
	if len(expected) != len(hands) {
		t.Error("INVALID TEST lists should be equal length")
	}
	for i, s := range hands {
		if err := Checkf(getHandRank(s), expected[i]); err != nil {
			t.Error(err)
		}
	}
}
func TestMapHandToRanks(t *testing.T) {
	hands := []string{
		"AAAAA",
		"AA8AA",
		"23332",
		"TTT98",
		"23432",
		"A23A4",
		"23456",
	}
	expected := [][]int{
		{14, 14, 14, 14, 14},
		{14, 14, 8, 14, 14},
		{2, 3, 3, 3, 2},
		{10, 10, 10, 9, 8},
		{2, 3, 4, 3, 2},
		{14, 2, 3, 14, 4},
		{2, 3, 4, 5, 6},
	}
	// Make sure the test is set up properly
	if len(expected) != len(hands) {
		t.Error("INVALID TEST lists should be equal length")
	}
	for i, s := range hands {
		m := makeMapper(cardMap)
		if err := Checkf(m(s), expected[i]); err != nil {
			t.Error(err)
		}
	}
}
