package day08

import (
	"log"
	"regexp"

	"gitlab.com/joedowning27/adventofcode/utils"
)

/*
In this problem we are being asked to walk a network of nodes
given a set of directions. Starting from the node AAA we need
to navigate to the end node ZZZ.

I am going to represent this graph using a map. The key to this
map will be the name of the node and the value will be the node's
neighbors. For example:

AAA = (BBB, BBB)
BBB = (AAA, ZZZ)
ZZZ = (ZZZ, ZZZ)

will be represented by the following map:

	var map = map[string][]string{
		"AAA": {"BBB", "BBB"},
		"BBB": {"AAA", "ZZZ"},
		"ZZZ": {"ZZZ", "ZZZ"},
	}
*/
type wastelandMap map[string][]string

// max recursive distance just so we don't hang if I make a mistake
const MAX_RECURSE = 1000000000

/*
To keep track of the current instruction we can use the depth. Since
this is the problem solution it is already a value we are keeping
track of.
The first instruction we want to execute will be at index 0 when
the depth is 0. When we need to loop back around to the first index
we can just use the % operator:
	InstIdx = dist % len(Insts)
*/

func GetHauntedWastelandPart1(in []string) int {
	// parse our input
	// the first line is out instructions
	inst := in[0]

	// ignoring the second line the remaining lines are our map
	m := parseMap(in[2:])

	// starting from AAA walk the graph recursively and report the distance
	// dist := walkGraph(inst, 0, "AAA", m)
	dist := walkGraph(inst, 0, "AAA", m)
	return dist
}

/*
For part 2 we need to traverse multiple paths simultaneously
(6 based on the problem input). We could do this in parallel
Using goroutines and wait groups, but I'm just gonna do it
synchronously my changing my function signature to work on
slices.
We are only doing an array lookup and a handful of
comparisons so I think this should still be fast enough
*/
func GetHauntedWastelandPart2(in []string) int {
	// parse our input
	// the first line is out instructions
	inst := in[0]

	// ignoring the second line the remaining lines are our map
	m := parseMap(in[2:])

	// find all of the starting positions
	starts := getStarts(m)

	// starting from AAA walk the graph recursively and report the distance
	dists := walkGraphs(inst, 0, starts, m)

	// each path will eventually cycle try taking the LCM of all distances
	return utils.LCM(dists[0], dists[1], dists[2:]...)
}

// use regex to parse the nodes:
func parseMap(nodes []string) wastelandMap {
	m := wastelandMap{}
	nodeRE := regexp.MustCompile(`(.{3}) = \((.{3}), (.{3})\)`)
	// parse the nodes for each line
	for _, str := range nodes {
		if match := nodeRE.FindStringSubmatch(str); match != nil {
			m[match[1]] = match[2:]
		}
	}
	return m
}

func getStarts(m wastelandMap) []string {
	starts := []string{}
	for k := range m {
		if k[2] == 'A' {
			starts = append(starts, k)
		}
	}
	return starts
}

// walk the graph at each step:
//
//	increment the distance walked
//		instruction index can be inferred from the distance traveled
//			instIdx = dist % len(inst)
//	pick the next node from the map
//	base case is ZZZ

func walkGraphs(inst string, dist int, locs []string, m wastelandMap) []int {
	dists := []int{}
	for _, loc := range locs {
		dists = append(dists, walkGraph(inst, dist, loc, m))
	}
	return dists
}

func walkGraph(inst string, dist int, loc string, m wastelandMap) int {
	// we hit our distance limit time to blow up
	if dist >= MAX_RECURSE {
		log.Fatalf("Distance limit encountered %d\n", dist)
	}
	// if we are not at ZZZ continue
	if loc[2] != 'Z' {
		var next string
		i := inst[dist%len(inst)]
		switch i {
		case 'L':
			next = m[loc][0]
		case 'R':
			next = m[loc][1]
		default:
			log.Fatalf("Invalid instruction %c\n", i)
		}
		return walkGraph(inst, dist+1, next, m)
	}
	return dist
}
