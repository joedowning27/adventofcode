package day08

import (
	"fmt"
	"reflect"
	"slices"
	"testing"
)

func Check(t *testing.T, actual, expected any) {
	// fmt.Printf("Comparing %v == %v\n", expected, actual)
	if !reflect.DeepEqual(expected, actual) {
		t.Errorf(
			`Check failed:
Actual: "%d"
Expected: "%d"`, actual, expected)
	}
}

func Checkf(actual, expected any) error {
	if !reflect.DeepEqual(expected, actual) {
		return fmt.Errorf("Check failed:\n Actual: \"%d\"\nExpected: \"%d\"\n", actual, expected)
	}
	return nil
}

var in1 = []string{
	"RL",
	"",
	"AAA = (BBB, CCC)",
	"BBB = (DDD, EEE)",
	"CCC = (ZZZ, GGG)",
	"DDD = (DDD, DDD)",
	"EEE = (EEE, EEE)",
	"GGG = (GGG, GGG)",
	"ZZZ = (ZZZ, ZZZ)",
}
var in2 = []string{
	"LLR",
	"",
	"AAA = (BBB, BBB)",
	"BBB = (AAA, ZZZ)",
	"ZZZ = (ZZZ, ZZZ)",
}

func TestGetHauntedWastelandPart1(t *testing.T) {
	// simple two step solution
	out := GetHauntedWastelandPart1(in1)
	if err := Checkf(out, 2); err != nil {
		t.Error(err)
	}
	// need to recursively iterate through the loop here
	out = GetHauntedWastelandPart1(in2)
	if err := Checkf(out, 6); err != nil {
		t.Error(err)
	}
}

var in3 = []string{
	"LR",
	"",
	"11A = (11B, XXX)",
	"11B = (XXX, 11Z)",
	"11Z = (11B, XXX)",
	"22A = (22B, XXX)",
	"22B = (22C, 22C)",
	"22C = (22Z, 22Z)",
	"22Z = (22B, 22B)",
	"XXX = (XXX, XXX)",
}

func TestGetHauntedWastelandPart2(t *testing.T) {
	// simple two step solution
	out := GetHauntedWastelandPart2(in3)
	if err := Checkf(out, 6); err != nil {
		t.Error(err)
	}
}

func TestParseMap(t *testing.T) {
	out := parseMap(in2[2:])
	expected := wastelandMap{
		"AAA": {"BBB", "BBB"},
		"BBB": {"AAA", "ZZZ"},
		"ZZZ": {"ZZZ", "ZZZ"},
	}
	if err := Checkf(out, expected); err != nil {
		t.Error(err)
	}
}

func TestWalkGraph(t *testing.T) {
	m := wastelandMap{
		"AAA": {"BBB", "BBB"},
		"BBB": {"AAA", "ZZZ"},
		"ZZZ": {"ZZZ", "ZZZ"},
	}
	// recursive graph walk
	out := walkGraph("LLR", 0, "AAA", m)
	if err := Checkf(out, 6); err != nil {
		t.Error(err)
	}

	// Base Case ZZZ
	out = walkGraph("LLR", 5, "ZZZ", m)
	if err := Checkf(out, 5); err != nil {
		t.Error(err)
	}

}

func TestGetStarts(t *testing.T) {
	m := wastelandMap{
		"AAA": {"BBB", "BBB"},
		"BBB": {"AAA", "ZZZ"},
		"ZZZ": {"ZZZ", "ZZZ"},
		"BBA": {"ZZZ", "ZZZ"},
	}
	expect := []string{"AAA", "BBA"}
	out := getStarts(m)

	//sort both values so we can use deepequal
	slices.Sort(expect)
	slices.Sort(out)

	if err := Checkf(out, expect); err != nil {
		t.Error(err)
	}
}
