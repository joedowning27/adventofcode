package day09

import (
	"slices"

	"gitlab.com/joedowning27/adventofcode/utils"
)

/*
our puzzle input from the OASIS device gives us a value
that changes over time. our job is to predict the next value.

This can by done by creating a sort of inverted pyramid. The
values in each layer are the differences of neighboring values
in the layer above. When a layer is all 0 then we should stop early

	data[N][i] = abs(data[N-1][i] - data[N-1][i+1])

0   3   6   9  12  15
  3   3   3   3   3
    0   0   0   0

This pyramid is just going to be represented as a two dimensional
slice of ints.
*/

// type oasisData [][]int

/*
After forming the pyramid we can use a backfilling technique to
find the next root value. Add a 0 to the lowest layer. To fill
the last value of a layer we can use the following equation.
Where

	data[N][len(data[N])] = last(data[N+1]) + last(data[N])

0   3   6   9  12  15  *18*
  3   3   3   3   3  *3*
    0   0   0   0  *0*

Our final puzzle solution is just the sum off all these predictions
*/

func GetMiragemaintenancePart1(in []string) int {
	acc := 0
	// for each set of input data points
	for _, v := range in {
		// parse the puzzle data into []int
		data := utils.GetInts(v)
		// ignore empty lines
		if len(data) <= 0 {
			continue
		}
		// run the oasis prediction and add to the acc
		acc += nextPrediction(data)
	}
	return acc
}

func GetMiragemaintenancePart2(in []string) int {
	acc := 0
	// for each set of input data points
	for _, v := range in {
		// parse the puzzle data into []int
		data := utils.GetInts(v)
		// ignore empty lines
		if len(data) <= 0 {
			continue
		}
		// run the oasis prediction and add to the acc
		slices.Reverse(data)
		acc += nextPrediction(data)
	}
	return acc
}

// lets try this recursively instead
func nextPrediction(ints []int) int {
	// the next value will just be the last(ints) + nextPrediction(diffs)
	diffs := []int{}
	for i := 0; i < len(ints)-1; i++ {
		diffs = append(diffs, ints[i+1]-ints[i])
	}

	// if any diff is not 0 we need to check another layer
	for _, v := range diffs {
		if v != 0 {
			return ints[len(ints)-1] + nextPrediction(diffs)
		}
	}
	//otherwise our answer is just the last value in ints
	// ints = []ints{..., x, x, x}
	// diffs = []ints{..., 0, 0}
	return ints[len(ints)-1]
}
