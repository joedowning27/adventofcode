package day09

import (
	"fmt"
	"reflect"
	"testing"
)

func Checkf(actual, expected any) error {
	if !reflect.DeepEqual(expected, actual) {
		return fmt.Errorf("Check failed:\n Actual: \"%d\"\nExpected: \"%d\"\n", actual, expected)
	}
	return nil
}

var in = []string{
	"0 3 6 9 12 15",
	"1 3 6 10 15 21",
	"10 13 16 21 30 45",
}

func TestGetMiragemaintenancePart1(t *testing.T) {
	out := GetMiragemaintenancePart1(in)
	if err := Checkf(out, 114); err != nil {
		t.Error(err)
	}
}

func TestNextPrediction(t *testing.T) {
	out := nextPrediction([]int{0, 3, 6, 9, 12, 15})
	if err := Checkf(out, 18); err != nil {
		t.Error(err)
	}
	out = nextPrediction([]int{1, 3, 6, 10, 15, 21})
	if err := Checkf(out, 28); err != nil {
		t.Error(err)
	}
	out = nextPrediction([]int{10, 13, 16, 21, 30, 45})
	if err := Checkf(out, 68); err != nil {
		t.Error(err)
	}
}
