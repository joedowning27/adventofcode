package day10

import (
	"log"

	"gitlab.com/joedowning27/adventofcode/utils"
)

/*
In this puzzle we will be given a bunch of symbols that
correspond to different pipes in a maze.
Those symbols include
	| is a vertical pipe connecting north and south.
	- is a horizontal pipe connecting east and west.
	L is a 90-degree bend connecting north and east.
	J is a 90-degree bend connecting north and west.
	7 is a 90-degree bend connecting south and west.
	F is a 90-degree bend connecting south and east.
	. is ground; there is no pipe in this tile.
	S is the starting position of the animal.

These pipes will form a loop that starts and ends at S
Our goal is to find the distance to the tile that is
the farthest away from S in this loop.

	.....		.....
	.S-7.		.012.
	.|.|. 	=>	.1.3.
	.L-J.		.234.
	.....		.....

	(3, 3) is the farthest point with a distance of 4

	..F7.		..45.
	.FJ|.		.236.
	SJ.L7	=>	01.78
	|F--J		14567
	LJ...		23...

	(4, 2) is the farthest away with a distance of 8

Keep in mind that we will have to compete with a noisy input.
This second example could look like this instead:
	7-F7-
	.FJ|7
	SJLL7
	|F--J
	LJ.LJ

For this problem I think we should start by just doing a depth first
search. Starting from S we look out in all directions that connect to
S. We can prune any branches that do not point to S.
From there we can iterate through the graph until we hit one of
three base cases
	1. We arrive back at S
	2. We reach a Dead End
		A Dead End is a pipe that does not connect to its neighbors
		ie. S--.; - is a dead end in this graph
	3. Our path would put us out of the map


Keeping track of our distance we can find the length of this loop.
Then we know that the furthest we can reach on this loop is
distance / 2. If distance is odd we should round up

maxSteps = math.Ceil(dist/2)

*/

type maze [][]rune

type point struct {
	x, y int
}

func GetPipemazePart1(in []string) int {
	m := utils.Get2DRune(in)
	// find the location if S in the maze
	p := findStart(m)
	// for each direction walk the maze taking the longest
	dist := 0
	for _, dir := range []rune{'U', 'D', 'L', 'R'} {
		// walk the maze in that direction finding the longest path
		if d := walkMaze(p, dir, m); d > dist {
			dist = d
		}
	}
	// return half distance rounded up
	return dist/2 + dist%2
}

func GetPipemazePart2(in []string) int {
	m := utils.Get2DRune(in)
	// find the location if S in the maze
	p := findStart(m)
	// for each direction walk the maze taking the longest
	path := []point{}
	for _, dir := range []rune{'U', 'D', 'L', 'R'} {
		// walk the maze in that direction finding the longest path
		if p := pathMaze(p, dir, m, []point{}); len(p) > len(path) {
			path = p
		}
	}
	// return number of internal points
	return getInternalPoints(path)
}

// returns a slice [x, y]
func findStart(m maze) point {
	for y, row := range m {
		for x, p := range row {
			if p == 'S' {
				return point{x, y}
			}
		}
	}
	return point{-1, -1}
}

var moveMap = map[rune]map[rune]rune{
	// moving up the next should be F, 7 or |
	'U': {'F': 'R', '7': 'L', '|': 'U'},
	// moving down the next should be L, J, |
	'D': {'L': 'R', 'J': 'L', '|': 'D'},
	// moving left the next should be -, L, F
	'L': {'L': 'U', 'F': 'D', '-': 'L'},
	// moving right the next should be -, J, 7
	'R': {'7': 'D', 'J': 'U', '-': 'R'},
}

func walkMaze(p point, dir rune, m maze) int {
	return len(pathMaze(p, dir, m, []point{}))
}

// dir should be one of: U, D, L, R
func pathMaze(p point, dir rune, m maze, path []point) []point {
	// fmt.Printf("dist: %d dir: %c m(%d, %d): %c\n", dist, dir, x, y, m[y][x])
	path = append(path, p)
	// get the next space based on the direction we are facing
	switch dir {
	case 'U':
		// move up
		p.y--
	case 'D':
		// move down
		p.y++
	case 'L':
		// move left
		p.x--
	case 'R':
		// move right
		p.x++
	default:
		log.Fatalf("Invalid direction: '%c'\n", dir)
	}
	// is that space out of bounds
	if p.y < 0 || p.y >= len(m) {
		return nil
	}
	if p.x < 0 || p.x >= len(m[p.y]) {
		return nil
	}
	// is that space the final space S
	if m[p.y][p.x] == 'S' {
		return path
	}

	// get the next direction
	dir, found := moveMap[dir][rune(m[p.y][p.x])]
	if !found {
		// if the move does not exist this is a dead end
		return nil
	}

	// continue to search step to next space
	return pathMaze(p, dir, m, path)
}

func getInternalPoints(p []point) int {
	// Pick's theorem states
	// Area = internalpoints + perimiterpoints/2 - 1
	i := getShoelaceArea(p) + 1 - (len(p) / 2)
	return i
}

func getShoelaceArea(p []point) int {
	// using the shoelace formula for the area of a polygon
	tmp := 0
	path := append(p, p[0])
	for i := 0; i < len(path)-1; i++ {
		tmp += (path[i].y + path[i+1].y) * (path[i].x - path[i+1].x)
	}

	return utils.Abs(tmp / 2)
}
