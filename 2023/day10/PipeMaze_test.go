package day10

import (
	"fmt"
	"reflect"
	"testing"

	"gitlab.com/joedowning27/adventofcode/utils"
)

func Checkf(actual, expected any) error {
	if !reflect.DeepEqual(expected, actual) {
		return fmt.Errorf("Check failed:\nActual: \"%d\"\nExpected: \"%d\"\n", actual, expected)
	}
	return nil
}

var in1 = []string{
	".....",
	".S-7.",
	".|.|.",
	".L-J.",
	".....",
}
var in2 = []string{
	"..F7.",
	".FJ|.",
	"SJ.L7",
	"|F--J",
	"LJ...",
}

var in3 = []string{
	"7-F7-",
	".FJ|7",
	"SJLL7",
	"|F--J",
	"LJ.LJ",
}

func TestGetPipemazePart1(t *testing.T) {
	out := GetPipemazePart1(in1)
	if err := Checkf(out, 4); err != nil {
		t.Error(err)
	}
	out = GetPipemazePart1(in2)
	if err := Checkf(out, 8); err != nil {
		t.Error(err)
	}
	out = GetPipemazePart1(in3)
	if err := Checkf(out, 8); err != nil {
		t.Error(err)
	}
}

var part2in = []string{
	"...........",
	".S-------7.",
	".|F-----7|.",
	".||.....||.",
	".||.....||.",
	".|L-7.F-J|.",
	".|..|.|..|.",
	".L--J.L--J.",
	"...........",
}

var part2in2 = []string{
	".F----7F7F7F7F-7....",
	".|F--7||||||||FJ....",
	".||.FJ||||||||L7....",
	"FJL7L7LJLJ||LJ.L-7..",
	"L--J.L7...LJS7F-7L7.",
	"....F-J..F7FJ|L7L7L7",
	"....L7.F7||L7|.L7L7|",
	".....|FJLJ|FJ|F7|.LJ",
	"....FJL-7.||.||||...",
	"....L---J.LJ.LJLJ...",
}
var part2in3 = []string{
	"FF7FSF7F7F7F7F7F---7",
	"L|LJ||||||||||||F--J",
	"FL-7LJLJ||||||LJL-77",
	"F--JF--7||LJLJ7F7FJ-",
	"L---JF-JLJ.||-FJLJJ7",
	"|F|F-JF---7F7-L7L|7|",
	"|FFJF7L7F-JF7|JL---7",
	"7-L-JL7||F7|L7F-7F7|",
	"L.L7LFJ|||||FJL7||LJ",
	"L7JLJL-JLJLJL--JLJ.L",
}

func TestGetPipemazePart2(t *testing.T) {
	out := GetPipemazePart2(part2in)
	if err := Checkf(out, 4); err != nil {
		t.Error(err)
	}

	out = GetPipemazePart2(part2in2)
	if err := Checkf(out, 8); err != nil {
		t.Error(err)
	}

	out = GetPipemazePart2(part2in3)
	if err := Checkf(out, 10); err != nil {
		t.Error(err)
	}
}

func TestFindStart(t *testing.T) {
	out := findStart(utils.Get2DRune(in1))
	if err := Checkf(out, point{1, 1}); err != nil {
		t.Error(err)
	}
	out = findStart(utils.Get2DRune(in2))
	if err := Checkf(out, point{0, 2}); err != nil {
		t.Error(err)
	}
	out = findStart(utils.Get2DRune(in3))
	if err := Checkf(out, point{0, 2}); err != nil {
		t.Error(err)
	}
}

func TestWalkMaze(t *testing.T) {
	out := walkMaze(point{1, 1}, 'R', utils.Get2DRune(in1))
	if err := Checkf(out, 8); err != nil {
		t.Error(err)
	}
	out = walkMaze(point{0, 2}, 'R', utils.Get2DRune(in2))
	if err := Checkf(out, 16); err != nil {
		t.Error(err)
	}
	out = walkMaze(point{0, 2}, 'R', utils.Get2DRune(in3))
	if err := Checkf(out, 16); err != nil {
		t.Error(err)
	}
}

func TestPathMaze(t *testing.T) {
	expected := []point{
		{1, 1}, {2, 1}, {3, 1}, // right
		{3, 2}, {3, 3}, // down
		{2, 3}, {1, 3}, // left
		{1, 2}, // up
	}
	out := pathMaze(point{1, 1}, 'R', utils.Get2DRune(in1), []point{})
	if err := Checkf(out, expected); err != nil {
		t.Error(err)
	}
}

func TestShoelaceArea(t *testing.T) {
	path := []point{
		{1, 1}, {2, 1}, {3, 1}, // right
		{3, 2}, {3, 3}, // down
		{2, 3}, {1, 3}, // left
		{1, 2}, // up
	}
	out := getShoelaceArea(path)
	if err := Checkf(out, 4); err != nil {
		t.Error(err)
	}

	// ...........
	// .S-------7.
	// .|F-----7|.
	// .||.....||.
	// .||.....||.
	// .|L-7.F-J|.
	// .|..|.|..|.
	// .L--J.L--J.
	// ...........
	p2 := []point{
		{1, 1}, {9, 1}, {9, 7}, {6, 7},
		{6, 5}, {8, 5}, {8, 2}, {2, 2},
		{2, 5}, {4, 5}, {4, 7}, {1, 7},
	}
	out = getShoelaceArea(p2)
	if err := Checkf(out, 26); err != nil {
		t.Error(err)
	}
}

func TestGetInternalPoints(t *testing.T) {
	path := []point{
		{1, 1}, {2, 1}, {3, 1}, // right
		{3, 2}, {3, 3}, // down
		{2, 3}, {1, 3}, // left
		{1, 2}, // up
	}
	out := getInternalPoints(path)
	if err := Checkf(out, 1); err != nil {
		t.Error(err)
	}

}
