package day11

import (
	"strings"

	"gitlab.com/joedowning27/adventofcode/utils"
)

/*
In the first part of this we need to take a map and expand it.
Any row or col without a # needs to be doubled

This:

   v  v  v
 ...#......
 .......#..
 #.........
>..........<
 ......#...
 .#........
 .........#
>..........<
 .......#..
 #...#.....
   ^  ^  ^

Becomes this:

....#........
.........#...
#............
.............
.............
........#....
.#...........
............#
.............
.............
.........#...
#....#.......


The second part needs to find the shortest distance between
all paris of galaxies.

The above map should have 36 pairs of galaxies

Our distances must move one step at a time. These two are the same

....1........		....1........
.........2...		.........2...
3............		3............
.............		.............
.............		.............
........4....		........4....
.5...........   ->	.5####.......
.##.........6		.....#......6
..##.........		.....#.......
...##........		.....#.......
....##...7...		.....#...7...
8....9.......		8....9.......

Between galaxy 5 and galaxy 9: 9
Between galaxy 1 and galaxy 7: 15
Between galaxy 3 and galaxy 6: 17
Between galaxy 8 and galaxy 9: 5
N: x, y
5: 1, 6
9: 5, 11

d = (y2 - y1) + (x2 - x1)
d = (11 - 6) + (5 - 1)
d = 5 + 4 = 9

1: 4, 0
7: 9, 10
d = (10 - 0) + (9 - 4) = 10 + 5 = 15

The solution for Part1 is the sum of these distance pairs
*/

type point struct {
	x, y int
}

func (a point) dist(b point) int {
	v := utils.Abs(a.x-b.x) + utils.Abs(a.y-b.y)
	return v
}

func GetCosmicExpansionPart1(in []string) int {
	// parse the input into a [][]rune
	m := utils.Get2DRune(in)
	// expand the universe
	m = expandUniverse(m)
	// find all galaxies
	gs := findGalaxies(m, 2)
	// calculate the distance between each pair
	return calcDists(gs)
}

func GetCosmicExpansionPart2(in []string) int {
	// parse the input into a [][]rune
	m := utils.Get2DRune(in)
	// expand the universe
	m = mapUniverse(m)
	// find all galaxies
	gs := findGalaxies(m, 1000000)
	// calculate the distance between each pair
	return calcDists(gs)
}

func calcDists(gs []point) int {
	acc := 0
	for i, start := range gs {
		// match with all gs after this one
		for _, end := range gs[i:] {
			acc += start.dist(end)
		}
	}
	return acc
}

func expandUniverse(in [][]rune) [][]rune {
	m := [][]rune{}
	// expand X
	for x := 0; x < len(in[0]); x++ {
		col := []rune{}
		for y := 0; y < len(in); y++ {
			col = append(col, in[y][x])
		}
		for y := 0; y < len(in); y++ {
			if len(m) <= y {
				m = append(m, []rune{})
			}
			m[y] = append(m[y], in[y][x])
		}
		if !strings.Contains(string(col), "#") {
			for y := 0; y < len(in); y++ {
				m[y] = append(m[y], in[y][x])
			}
		}
	}
	// expand Y
	out := [][]rune{}
	for i := 0; i < len(m); i++ {
		if !strings.Contains(string(m[i]), "#") {
			out = append(out, m[i])
		}
		out = append(out, m[i])
	}
	return out
}

// replace the "expansion" sections with placeholders
// we will do some math to calculate the distance later
func mapUniverse(in [][]rune) [][]rune {
	for x := 0; x < len(in[0]); x++ {
		col := []rune{}
		// accumulate this column
		for y := range in {
			col = append(col, in[y][x])
		}
		// if there are no galaxies replace this col
		if !strings.Contains(string(col), "#") {
			for y := range in {
				in[y][x] = 'V'
			}
		}
	}
	for y, row := range in {
		// if there are no galaxies in this row replace
		if !strings.Contains(string(row), "#") {
			s := strings.ReplaceAll(string(row), ".", "H")
			in[y] = []rune(s)
		}
	}
	return in
}

func findGalaxies(in [][]rune, dist int) []point {
	p := []point{}
	y := 0
	for _, row := range in {
		x := 0
		if row[0] == 'H' {
			y += dist
			continue
		}
		for _, v := range row {
			switch v {
			case '#':
				p = append(p, point{x, y})
				x++
			case 'V':
				x += dist
			case '.':
				x++
			}
		}
		y++
	}
	return p
}
