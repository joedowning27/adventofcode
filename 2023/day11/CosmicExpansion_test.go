package day11

import (
	"fmt"
	"reflect"
	"testing"

	"gitlab.com/joedowning27/adventofcode/utils"
)

func Checkf(actual, expected any) error {
	if !reflect.DeepEqual(expected, actual) {
		return fmt.Errorf("Check failed:\n Actual: \"%d\"\nExpected: \"%d\"\n", actual, expected)
	}
	return nil
}

var in = []string{
	"...#......",
	".......#..",
	"#.........",
	"..........",
	"......#...",
	".#........",
	".........#",
	"..........",
	".......#..",
	"#...#.....",
}

func TestGetCosmicExpansionPart1(t *testing.T) {
	out := GetCosmicExpansionPart1(in)
	if err := Checkf(out, 374); err != nil {
		t.Error(err)
	}
}

var expanded1 = utils.Get2DRune([]string{
	"....#........",
	".........#...",
	"#............",
	".............",
	".............",
	"........#....",
	".#...........",
	"............#",
	".............",
	".............",
	".........#...",
	"#....#.......",
})

func TestExpandUniverse(t *testing.T) {
	out := expandUniverse(utils.Get2DRune(in))
	if err := Checkf(out, expanded1); err != nil {
		for _, row := range out {
			for _, v := range row {
				fmt.Printf("%c", v)
			}
			fmt.Printf("\n")
		}
		t.Error(err)
	}
}

var universe = utils.Get2DRune([]string{
	"..V#.V..V.",
	"..V..V.#V.",
	"#.V..V..V.",
	"HHVHHVHHVH",
	"..V..V#.V.",
	".#V..V..V.",
	"..V..V..V#",
	"HHVHHVHHVH",
	"..V..V.#V.",
	"#.V.#V..V.",
})

func TestMapUniverse(t *testing.T) {
	out := mapUniverse(utils.Get2DRune(in))
	if err := Checkf(out, universe); err != nil {
		t.Error(err)
	}
}

var g2 = []point{
	{4, 0}, {9, 1}, {0, 2}, {8, 5}, {1, 6},
	{12, 7}, {9, 10}, {0, 11}, {5, 11},
}

// var g10 = []point{}

func TestFindGalaxies(t *testing.T) {
	out := findGalaxies(expanded1, 2)
	if err := Checkf(out, g2); err != nil {
		t.Error(err)
	}
	out = findGalaxies(universe, 2)
	if err := Checkf(out, g2); err != nil {
		t.Error(err)
	}
}

func TestPointDistance(t *testing.T) {
	// 5 -> 9 = 9
	d1 := g2[4].dist(g2[8])
	if err := Checkf(d1, 9); err != nil {
		t.Error(err)
	}
	// 1 -> 7 = 15
	d2 := g2[0].dist(g2[6])
	if err := Checkf(d2, 15); err != nil {
		t.Error(err)
	}
	// 3 -> 6 = 17
	d3 := g2[2].dist(g2[5])
	if err := Checkf(d3, 17); err != nil {
		t.Error(err)
	}
	// 8 -> 9 = 5
	d4 := g2[7].dist(g2[8])
	if err := Checkf(d4, 5); err != nil {
		t.Error(err)
	}

}

func TestCalcDists(t *testing.T) {
	g10 := findGalaxies(universe, 10)
	out := calcDists(g10)
	if err := Checkf(out, 1030); err != nil {
		t.Error(err)
	}

	g100 := findGalaxies(universe, 100)
	out = calcDists(g100)
	if err := Checkf(out, 8410); err != nil {
		t.Error(err)
	}
}
