package day12

import (
	"fmt"
	"log"
	"strings"

	"gitlab.com/joedowning27/adventofcode/utils"
)

func GetHotSpringsPart1(in []string) int {
	acc := 0
	cache := make(map[string]int)
	for _, v := range in {
		s := parseSpringData(v)
		acc += findConfigsDP(cache, s)
	}
	return acc
}

func GetHotSpringsPart2(in []string) int {
	acc := 0
	cache := make(map[string]int)
	for _, v := range in {
		s := unfoldConfig(parseSpringData(v), 5)
		acc += findConfigsDP(cache, s)
	}
	return acc
}

type springData struct {
	layout string
	groups []int
}

func (d springData) String() string {
	return d.layout + " " + strings.Trim(strings.Replace(fmt.Sprint(d.groups), " ", ",", -1), "[]")
}

func parseSpringData(s string) springData {
	// input is in the form: "([?.#]+)\w([\d]+,?)+"
	// "???.### 1,1,3"
	strs := strings.Split(s, " ")
	if len(strs) != 2 {
		log.Fatalf("Cannot parse: incorrect number of spaces '%s'\n", s)
	}
	return springData{strs[0], utils.GetInts(strs[1])}
}

/*
This problem can be solved using dynamic programming
I am going to be taking the top-down/memoization approach.

This approach works by using recursion to break the top level
problem into smaller parts. A cache is passed along the recursion
so that similar solutions can me memorized and stored for later.
This will help avoid computing the same solution twice.

For our purposes the key to our cache is going to be a Stringified
version of our springData struct. This is because the `springData`
struct contains a reference type as one of the fields therefore it
cannot be used as the key to the map.

NOTE: maps are reference types so they are passed by reference and
further recursive calls will all modify the same map

DP:
	Base Cases:
		1. s.groups == []
			if layout.contains("#")
				return 0 // we are out of groups but still springs remain
			else
				return 1 // we have matched all groups to springs
		2. len(layout) < sum(groups)
			return 0 // there are not enough springs to match all groups
		3. result is in the map
			return cache[s.String()]
	Recursion:
		cur_group = groups[0]
		1. First of layout is "." we can ignore and move to next step
			if layout[0] == "."
				return findConfigs(cache, springData{s.layout[1:], s.groups})
		2. "Fit" a group of springs into the layout
			group_fits 	= !(layout[0:cur_group].contains("."))
			equal_len 	= len(layout) == cur_group
			valid_last 	= layout[cur_group:cur_group+1] != "#"
			fits = group_fits && (equal_len || valid_last)
			if fits
				len = min(len(layout), cur_group + 1)
				total += findConfigs(cache, springData{s.layout[len:], s.groups[1:]})
		3. If the first is a "?" try another recursive path
			if layout[0] == "?"
				total += findConfigs(cache, springData{s.layout[1:], s.groups})
		cache[s.String()] = total
		return total
*/

func findConfigsDP(cache map[string]int, s springData) int {
	// Base Cases
	// we are out of groups to place
	if len(s.groups) <= 0 {
		// if there are no more # then we have found a valid config
		if strings.Contains(s.layout, "#") {
			return 0
		} else {
			// no # this must be a match
			return 1
		}
	}
	// check if there are enough characters to match
	if len(s.layout) < utils.Sum(s.groups) {
		return 0
	}
	// check if the solution is already in the cache
	if v, ok := cache[s.String()]; ok {
		return v
	}
	// Recursion
	// ignore . if it is the front of layout
	if s.layout[0] == '.' {
		return findConfigsDP(cache, springData{s.layout[1:], s.groups})
	}
	total := 0

	// check if we can fit the current group on the front of the layout
	curGroup := s.groups[0]
	// the first n letters cannot be .
	groupFits := !(strings.Contains(s.layout[0:curGroup], "."))
	// the group should be:
	// 		the same length as the remaining layout
	// 		or
	//		followed by a . or ?
	lastCheck := len(s.layout) == curGroup || s.layout[curGroup:curGroup+1] != "#"
	if groupFits && lastCheck {
		// fmt.Printf("Attempting to fit group size %d into '%s'\n", curGroup, s.layout)
		max_len := utils.IntMin(len(s.layout), curGroup+1)
		// fmt.Printf("maxLen: %d\n", max_len)
		total += findConfigsDP(cache, springData{s.layout[max_len:], s.groups[1:]})
	}

	// if layout starts with ? we can check for a solution that starts later
	if s.layout[0] == '?' {
		total += findConfigsDP(cache, springData{s.layout[1:], s.groups})
	}

	// cache and return result
	cache[s.String()] = total
	return total
}

func unfoldConfig(s springData, folds int) springData {
	l := []string{}
	g := []int{}
	for i := 0; i < folds; i++ {
		l = append(l, s.layout)
		g = append(g, s.groups...)
	}
	return springData{strings.Join(l, "?"), g}
}
