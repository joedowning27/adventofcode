package day12

import (
	"fmt"
	"reflect"
	"testing"
)

func Checkf(actual, expected any) error {
	if !reflect.DeepEqual(expected, actual) {
		return fmt.Errorf("Check failed:\nActual: \"%d\"\nExpected: \"%d\"\n", actual, expected)
	}
	return nil
}

var in = []string{
	"???.### 1,1,3",
	".??..??...?##. 1,1,3",
	"?#?#?#?#?#?#?#? 1,3,1,6",
	"????.#...#... 4,1,1",
	"????.######..#####. 1,6,5",
	"?###???????? 3,2,1",
}

var configs = []springData{
	{"???.###", []int{1, 1, 3}},
	{".??..??...?##.", []int{1, 1, 3}},
	{"?#?#?#?#?#?#?#?", []int{1, 3, 1, 6}},
	{"????.#...#...", []int{4, 1, 1}},
	{"????.######..#####.", []int{1, 6, 5}},
	{"?###????????", []int{3, 2, 1}},
}

func TestGetHotSpringsPart1(t *testing.T) {
	out := GetHotSpringsPart1(in)
	if err := Checkf(out, 21); err != nil {
		t.Error(err)
	}
}

func TestGetHotSpringsPart2(t *testing.T) {
	out := GetHotSpringsPart2(in)
	if err := Checkf(out, 525152); err != nil {
		t.Error(err)
	}
}

func TestParseSpringData(t *testing.T) {
	if len(in) != len(configs) {
		t.Error("Invalid Test: len(in) != len(configs)")
	}
	for i, v := range in {
		out := parseSpringData(v)
		if err := Checkf(out, configs[i]); err != nil {
			t.Error(err)
		}
	}
}

func TestFindConfigsBaseCases(t *testing.T) {
	cachedData := springData{"########", []int{7}}
	tests := []springData{
		{".#.", []int{}},     // emptyHash
		{".?.", []int{}},     // emptyNoHash
		{"###", []int{4}},    // bigGroup
		{"#.#", []int{1, 3}}, // bigGroups
		cachedData,           // cached
	}
	cache := make(map[string]int)
	cache[cachedData.String()] = 999
	expect := []int{0, 1, 0, 0, 999}

	if len(tests) != len(expect) {
		t.Error("Invalid Test: len(tests) != len(expect)")
	}
	for i, v := range tests {
		out := findConfigsDP(cache, v)
		if err := Checkf(out, expect[i]); err != nil {
			t.Error(err)
		}
	}
}

func TestFindConfigs(t *testing.T) {
	expect := []int{1, 4, 1, 1, 4, 10}
	if len(in) != len(expect) {
		t.Error("Invalid Test: len(configs) != len(expect)")
	}
	for i, v := range configs {
		out := findConfigsDP(make(map[string]int), v)
		if err := Checkf(out, expect[i]); err != nil {
			t.Error(err)
		}
	}
}

func TestSpringDataStringer(t *testing.T) {
	if len(in) != len(configs) {
		t.Error("Invalid Test: len(in) != len(configs)")
	}
	for i, v := range configs {
		out := v.String()
		if err := Checkf(out, in[i]); err != nil {
			t.Error(err)
		}
	}
}

var folded = []springData{
	{".#", []int{1}},
	{"???.###", []int{1, 1, 3}},
}
var unfolded = []springData{
	{".#?.#?.#?.#?.#", []int{1, 1, 1, 1, 1}},
	{"???.###????.###????.###????.###????.###", []int{1, 1, 3, 1, 1, 3, 1, 1, 3, 1, 1, 3, 1, 1, 3}},
}

func TestUnfoldConfig(t *testing.T) {
	if len(folded) != len(unfolded) {
		t.Error("Invalid Test: len(folded) != len(unfolded)")
	}
	for i := range folded {
		out := unfoldConfig(folded[i], 5)
		if err := Checkf(out, unfolded[i]); err != nil {
			t.Error(err)
		}
	}
}

func TestFindUnfoldedConfigs(t *testing.T) {
	expect := []int{1, 16384, 1, 16, 2500, 506250}
	if len(in) != len(expect) {
		t.Error("Invalid Test: len(configs) != len(expect)")
	}
	for i, v := range configs {
		u := unfoldConfig(v, 5)
		out := findConfigsDP(make(map[string]int), u)
		if err := Checkf(out, expect[i]); err != nil {
			t.Error(err)
		}
	}
}
