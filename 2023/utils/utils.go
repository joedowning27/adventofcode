package utils

import (
	"regexp"
	"strconv"
)

// convert a string into a list of integers
func GetInts(in string) []int {
	intRE := regexp.MustCompile(`-?\d+`)
	matches := intRE.FindAllString(in, -1)
	ints := []int{}
	for _, str := range matches {
		seed, err := strconv.Atoi(str)
		if err != nil {
			panic(err)
		}
		ints = append(ints, seed)
	}
	return ints
}

// taken from the internet
// https://go.dev/play/p/SmzvkDjYlb
// greatest common divisor (GCD) via Euclidean algorithm
func GCD(a, b int) int {
	for b != 0 {
		t := b
		b = a % b
		a = t
	}
	return a
}

// find Least Common Multiple (LCM) via GCD
func LCM(a, b int, integers ...int) int {
	result := a * b / GCD(a, b)

	for i := 0; i < len(integers); i++ {
		result = LCM(result, integers[i])
	}

	return result
}

// write our own absolute value function for ints to avoid ugly casting
func Abs(i int) int {
	if i < 0 {
		return -1 * i
	}
	return i
}

func Get2DRune(in []string) [][]rune {
	m := [][]rune{}
	for i := range in {
		row := []rune{}
		for j := range in[i] {
			row = append(row, rune(in[i][j]))
		}
		if len(row) > 0 {
			m = append(m, row)
		}
	}
	return m
}

func Sum(in []int) int {
	acc := 0
	for _, v := range in {
		acc += v
	}
	return acc
}

func IntMin(a, b int) int {
	if a < b {
		return a
	}
	return b
}
